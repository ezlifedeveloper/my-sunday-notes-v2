@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
    @lang('Hello!')
@endif

{{-- Intro Lines --}}
<p>Reset password request received, please click the button below</p>
<br/>

{{-- Action Button --}}
@component('mail::button', ['url' => $url, 'color' => 'primary'])
Reset Password
@endcomponent

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('Regards'),<br>
{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang(
    "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
    'into your web browser: [:actionURL](:actionURL)',
    [
        'actionText' => 'Reset Password',
        'actionURL' => $url,
    ]
)
@endslot
@endisset
@endcomponent
