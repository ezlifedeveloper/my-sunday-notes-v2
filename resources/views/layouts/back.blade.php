<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-full bg-white snippet-html js-focus-visible"
    data-js-focus-visible style="scroll-behavior: smooth;">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        @isset($title)
            <title>{{ $title.' - '.config('app.name') }}</title>
        @else
            <title>{{ config('app.name') }}</title>
        @endif

        <!-- Favicon -->
		<link rel="shortcut icon" href="{{ url(asset('favicon.ico')) }}">

        <!-- Fonts -->
        <link rel="stylesheet" href="https://rsms.me/inter/inter.css">

        @vite(['resources/sass/app.scss', 'resources/js/app.js'])
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        @livewireStyles

        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

        @stack('styles')
        
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <body class="h-full font-sans leading-normal tracking-normal bg-gray-800">
        <div class="min-h-full bg-gray-50">
            <x-admin.sidebar :title="$title">
                <main class="flex-1">
                    <div class="py-6">
                        <div class="px-4 mb-4 sm:px-6 md:pr-8">
                            <h1 class="text-2xl font-semibold text-gray-900 flex">
                                {!! $title !!}
                            </h1>
                        </div>
                        <div class="h-full px-4 sm:px-6 md:pr-8">
                            {{ $slot }}
                        </div>
                    </div>
                </main>
            </x-admin.sidebar>
        </div>

        @livewireScripts
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        
        <x-livewire-alert::scripts />      

        <script src="https://unpkg.com/@popperjs/core@2.9.1/dist/umd/popper.min.js" charset="utf-8"></script>
        <script type="text/javascript">
            var tooltipTriggerList = [].slice.call(
                document.querySelectorAll('[data-bs-toggle="tooltip"]')
            );
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new Tooltip(tooltipTriggerEl);
            });
        </script>
        @stack('scripts')

        <script>
            AOS.init();
        </script>
    </body>
</html>