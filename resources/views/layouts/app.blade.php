<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-full bg-white snippet-html js-focus-visible"
    data-js-focus-visible style="scroll-behavior: smooth;">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        @isset($title)
            <title>{{ $title.' - '.config('app.name') }}</title>
        @else
            <title>{{ config('app.name') }}</title>
        @endif

        <!-- Google Site Tags -->
        {{-- <meta name="google-site-verification" content="XLkLp57zdijWajfnB9HAy7JPnbq51Yptz9g0GIVl-vM" /> --}}
        
        <!-- Favicon -->
		<link rel="shortcut icon" href="{{ url(asset('favicon.ico')) }}">

        <!-- Google tag (gtag.js) -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-SP0L8Q3XVZ"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-SP0L8Q3XVZ');
        </script>

        <!-- SEO Tag -->
        <meta name="description" content="{{ $description }}">
        <meta name="author" content="Grasia Prima Perfekta">
        <meta property="og:site_name" content="My Sunday Notes" /> <!-- website name -->
	    <meta property="og:site" content="mysundaynotes.id" /> <!-- website link -->
	    <meta property="og:description" content="{{ $description }}" />
        <meta property="og:url" content="{{ Request::url() }}" />
        <meta property="og:image" content="{{ isset($imgasset) ? $imgasset : asset('msn.png') }}" />
        <meta property="og:title" content="{{ isset($title) ? __($title).' - '.env('APP_NAME') : env('APP_NAME') }}" />
        <meta property="og:type" content="{{ $type ?? 'productivity' }}" />
        <meta property="og:locale:alternate" content="en_US" />

        <!-- Fonts -->
        <link rel="stylesheet" href="https://rsms.me/inter/inter.css">

        @vite(['resources/sass/app.scss', 'resources/js/app.js'])
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        @livewireStyles

        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

        @stack('styles')
        
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <body class="antialiased font-sans">
        <div class="flex flex-col relative overflow-hidden min-h-screen">
            {{-- <div> --}}
                <x-header :title="$title ?? '-'"></x-header>
                    
                <main>
                    {{ $slot }}
                </main>
            {{-- </div> --}}

            <x-footer></x-footer>
        </div>

        @livewireScripts


        <script src="https://unpkg.com/@popperjs/core@2.9.1/dist/umd/popper.min.js" charset="utf-8"></script>
        <script type="text/javascript">
            var tooltipTriggerList = [].slice.call(
                document.querySelectorAll('[data-bs-toggle="tooltip"]')
            );
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new Tooltip(tooltipTriggerEl);
            });
        </script>
        @stack('scripts')

        <script>
            AOS.init();
        </script>
    </body>
</html>