<div>
    <div class="bg-gray-900 pt-10 sm:pt-16 relative overflow-hidden lg:pt-8 lg:pb-14">
        <div><img class="absolute bottom-0 left-1/2 w-[1440px] max-w-none -translate-x-1/2" src="https://tailwindui.com/img/component-images/grid-blur-purple-on-black.jpg" alt=""></div>
        <div class="relative mx-auto max-w-7xl lg:px-8">
            <div class="lg:grid lg:grid-cols-2 lg:gap-8">
                <div class="mx-auto max-w-md px-4 sm:max-w-2xl sm:px-6 sm:text-center lg:flex lg:items-center lg:px-0 lg:text-left">
                    <div class="lg:py-24">
                        <a href="{{ route('front.career') }}" class="group inline-flex items-center rounded-full bg-black p-1 pr-2 text-white hover:text-gray-200 hover:bg-white sm:text-base lg:text-sm xl:text-base" data-aos="fade-down" data-aos-delay="350">
                            <span class="rounded-full bg-primary px-3 py-0.5 text-sm font-semibold leading-5 text-white group-hover:bg-green-500">We're hiring</span>
                            <span class="ml-4 text-sm group-hover:text-primary">Visit our careers page</span>
                            <svg class="ml-2 h-5 w-5 text-gray-500" x-description="Heroicon name: mini/chevron-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M7.21 14.77a.75.75 0 01.02-1.06L11.168 10 7.23 6.29a.75.75 0 111.04-1.08l4.5 4.25a.75.75 0 010 1.08l-4.5 4.25a.75.75 0 01-1.06-.02z" clip-rule="evenodd"></path>
                            </svg>
                        </a>
                        <h1 class="mt-4 text-4xl font-bold tracking-tight text-white sm:mt-5 sm:text-6xl lg:mt-6 xl:text-6xl" data-aos="fade-right" data-aos-delay="450">
                            <span class="block">Find churches</span>
                            <span class="block text-primary">in one click</span>
                        </h1>
                        {{-- <p class="mt-3 text-base text-gray-300 sm:mt-5 sm:text-xl lg:text-lg xl:text-xl">Our very own database will help you with the personal detail of the church, combined with the powerful google maps and our note to write sermon or preaches in your app.</p> --}}
                        <div class="mt-5 sm:mt-6" data-aos="fade-down" data-aos-delay="450">
                            <div class="sm:mx-auto sm:max-w-xl lg:mx-0">
                                <div class="sm:flex">
                                    <div class="min-w-0 flex-1">
                                        <label for="churchName" class="sr-only">Church Name</label>
                                        <input type="text" wire:model.defer="search" placeholder="Enter church name" class="block w-full rounded-md border-0 px-4 py-3 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-300 focus:ring-offset-2 focus:ring-offset-gray-900">
                                    </div>
                                    <div class="mt-3 sm:mt-0 sm:ml-3">
                                        <button type="submit" wire:click="search" class="block w-full rounded-md bg-primary py-3 px-4 font-medium text-white shadow hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-indigo-300 focus:ring-offset-2 focus:ring-offset-gray-900">Search</button>
                                    </div>
                                </div>
                                <p class="mt-3 text-sm text-gray-300 sm:mt-4">
                                    Since 2018, our very own database will help you get the detail of the churches. By clicking the search button, you are agreed to our 
                                    <a href="{{ route('front.privacy-policy') }}" class="font-medium text-white">privacy policy</a>.
                                </p>
                            </div>
                            <div class="relative mx-auto mt-6">
                                <div class="mx-auto max-w-4xl">
                                    <dl class="rounded-lg bg-white shadow-lg sm:grid sm:grid-cols-3">
                                        <x-text.info-number :pos="'l'" :number="$user_count" :title="'Users'"/>
                                        <x-text.info-number :pos="'m'" :number="$church_count" :title="'Churches'"/>
                                        <x-text.info-number :pos="'r'" :number="$note_count" :title="'Notes'"/>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-12 -mb-16 sm:-mb-48 lg:relative lg:m-0 lg:ml-8" data-aos="fade-left" data-aos-delay="450">
                    <div class="mx-auto max-w-md px-4 sm:max-w-2xl sm:px-6 lg:max-w-none lg:px-0">
                        <img class="w-full lg:absolute lg:inset-y-0 lg:left-0 lg:h-full lg:w-auto lg:max-w-none" src="{{ asset('mysundaynotes.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="relative bg-white py-8 sm:py-16 lg:py-24">
        <div class="mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8">
            <h2 class="text-lg font-semibold text-cyan-600">An All-in-One Application</h2>
            <p class="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Everything you need to support your Sunday</p>
            {{-- <p class="mx-auto mt-5 max-w-prose text-xl text-gray-500">Phasellus lorem quam molestie id quisque diam aenean nulla in. Accumsan in quis quis nunc, ullamcorper malesuada. Eleifend condimentum id viverra nulla.</p> --}}
            <div class="mt-12">
                <div class="grid grid-cols-1 gap-8 sm:grid-cols-2 lg:grid-cols-3">
                    <x-text.vertical-with-icon-small :title="'Make Notes'" :detail="'With templated notes, you will not forget the points!'" :fade="'right'" :delay="'900'">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125" />
                    </x-text.vertical-with-icon-small>

                    <x-text.vertical-with-icon-small :title="'Church Data'" :detail="'Our church datas that you can count to search for nearby church and keep updated!'" :fade="'down'" :delay="'1000'">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M20.25 6.375c0 2.278-3.694 4.125-8.25 4.125S3.75 8.653 3.75 6.375m16.5 0c0-2.278-3.694-4.125-8.25-4.125S3.75 4.097 3.75 6.375m16.5 0v11.25c0 2.278-3.694 4.125-8.25 4.125s-8.25-1.847-8.25-4.125V6.375m16.5 0v3.75m-16.5-3.75v3.75m16.5 0v3.75C20.25 16.153 16.556 18 12 18s-8.25-1.847-8.25-4.125v-3.75m16.5 0c0 2.278-3.694 4.125-8.25 4.125s-8.25-1.847-8.25-4.125" />
                    </x-text.vertical-with-icon-small>
                
                    <x-text.vertical-with-icon-small :title="'Bible'" :detail="'Bible is provided to improve your experiences in making notes!'" :fade="'left'" :delay="'900'">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 6.042A8.967 8.967 0 006 3.75c-1.052 0-2.062.18-3 .512v14.25A8.987 8.987 0 016 18c2.305 0 4.408.867 6 2.292m0-14.25a8.966 8.966 0 016-2.292c1.052 0 2.062.18 3 .512v14.25A8.987 8.987 0 0018 18a8.967 8.967 0 00-6 2.292m0-14.25v14.25" />
                    </x-text.vertical-with-icon-small>
                
                    <x-text.vertical-with-icon-small :title="'Advanced Security'" :detail="'Your data is encrypted and stored safely in our system'" :fade="'right'" :delay="'900'">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75L11.25 15 15 9.75m-3-7.036A11.959 11.959 0 013.598 6 11.99 11.99 0 003 9.749c0 5.592 3.824 10.29 9 11.623 5.176-1.332 9-6.03 9-11.622 0-1.31-.21-2.571-.598-3.751h-.152c-3.196 0-6.1-1.248-8.25-3.285z" />
                    </x-text.vertical-with-icon-small>
                
                    <x-text.vertical-with-icon-small :title="'Powerful API'" :detail="'API to collaborate with other developer around the worlds'" :fade="'up'" :delay="'1000'">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.19 8.688a4.5 4.5 0 011.242 7.244l-4.5 4.5a4.5 4.5 0 01-6.364-6.364l1.757-1.757m13.35-.622l1.757-1.757a4.5 4.5 0 00-6.364-6.364l-4.5 4.5a4.5 4.5 0 001.242 7.244" />
                    </x-text.vertical-with-icon-small>
                
                    <x-text.vertical-with-icon-small :title="'Multi Devices'" :detail="'Your data is always available across your devices!'" :fade="'left'" :delay="'900'">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99" />
                    </x-text.vertical-with-icon-small>
                </div>
            </div>
        </div>
    </div>

    @livewire('front.app-review')

    <div class="relative py-8 sm:py-16 lg:py-24">
        <x-post.container :title="'Helpful Resources'" :category="'Learn'" :seeMore="true">
            <x-post.card-vertical :title="'Improve your customer experience'" :category="'Case Study'" 
                :description="'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis perferendis hic.'"></x-post.card-vertical>
            <x-post.card-vertical :title="'Improve your customer experience'" :category="'Case Study'" 
                :description="'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis perferendis hic.'"></x-post.card-vertical>
            <x-post.card-vertical :title="'Improve your customer experience'" :category="'Case Study'" 
                :description="'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis perferendis hic.'"></x-post.card-vertical>
        </x-post.container>
    </div>

    <x-footer.store-available></x-footer.store-available>
</div>