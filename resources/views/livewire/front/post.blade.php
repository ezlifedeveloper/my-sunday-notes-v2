<div>
    <x-header.small-blank-header></x-header.small-blank-header>
    <div class="bg-gray-900 pt-10 sm:pt-16 relative overflow-hidden lg:pt-8 pb-4">
        <div><img class="absolute bottom-0 left-1/2 w-[1440px] max-w-none -translate-x-1/2" src="https://tailwindui.com/img/component-images/grid-blur-purple-on-black.jpg" alt=""></div>
        <div class="relative mx-auto max-w-7xl px-8">
            <div class="mx-auto max-w-full">
                <div class="lg:py-8">
                    <h1 class="mt-4 text-4xl font-bold tracking-tight text-white sm:mt-5 sm:text-6xl lg:mt-6 xl:text-6xl">
                        <span class="block">Search for post</span>
                    </h1>
                    {{-- <p class="mt-3 text-base text-gray-300 sm:mt-5 sm:text-xl lg:text-lg xl:text-xl">Our very own database will help you with the personal detail of the church, combined with the powerful google maps and our note to write sermon or preaches in your app.</p> --}}
                    <div class="mt-5 sm:mt-6">
                        <form action="#" class="w-full">
                            <div class="sm:flex">
                                <div class="min-w-0 flex-1">
                                    <input type="text" wire:model="search" placeholder="Search something.." class="block w-full rounded-md border-0 px-4 py-3 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-300 focus:ring-offset-2 focus:ring-offset-gray-900">
                                </div>
                                <div class="mt-3 sm:mt-0 sm:ml-3">
                                    <button type="submit" class="block w-full rounded-md bg-primary py-3 px-4 font-medium text-white shadow hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-indigo-300 focus:ring-offset-2 focus:ring-offset-gray-900">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="relative lg:py-8">
        <x-post.container>
            <x-post.card-vertical :title="'Improve your customer experience'" :category="'Case Study'" 
                :description="'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis perferendis hic.'"></x-post.card-vertical>
            <x-post.card-vertical :title="'Improve your customer experience'" :category="'Case Study'" 
                :description="'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis perferendis hic.'"></x-post.card-vertical>
            <x-post.card-vertical :title="'Improve your customer experience'" :category="'Case Study'" 
                :description="'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis perferendis hic.'"></x-post.card-vertical>
            {{-- <x-post.card-vertical :title="'Improve your customer experience'" :category="'Case Study'" 
                :description="'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis perferendis hic.'"></x-post.card-vertical>
            <x-post.card-vertical :title="'Improve your customer experience'" :category="'Case Study'" 
                :description="'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis perferendis hic.'"></x-post.card-vertical>
            <x-post.card-vertical :title="'Improve your customer experience'" :category="'Case Study'" 
                :description="'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis perferendis hic.'"></x-post.card-vertical> --}}
        </x-post.container>
    </div>
</div>
