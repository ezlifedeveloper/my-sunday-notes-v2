<div>
    <x-header.small-blank-header></x-header.small-blank-header>
    <div class="mx-auto py-4 px-4 sm:py-24 sm:px-6 lg:py-16 lg:max-w-7xl lg:px-8">
        <!-- Product -->
        <div class="lg:grid lg:grid-cols-7 lg:grid-rows-1 lg:gap-x-8 lg:gap-y-10 xl:gap-x-16">
            <!-- Product image -->
            <div class="lg:col-span-4 lg:row-end-1" data-aos="fade-right" data-aos-delay="150">
                <div class="aspect-w-4 aspect-h-3 overflow-hidden rounded-lg bg-gray-100">
                <img src="{{ $data->image }}" alt="Sample of 30 icons with friendly and fun details in outline, filled, and brand color styles." class="object-cover object-center">
                </div>
            </div>

            <!-- Product details -->
            <div class="mx-auto mt-4 max-w-2xl sm:mt-16 lg:col-span-3 lg:row-span-2 lg:row-end-2 lg:mt-0 lg:max-w-none">
                <div class="flex flex-col-reverse">
                    <div>
                        <h1 class="text-2xl font-bold tracking-tight text-gray-900 sm:text-3xl flex flex-row gap-2" 
                            data-aos="fade-left" data-aos-delay="350">
                            {{ $data->name }}
                            @if($data->is_verified)
                            <div class="text-green-600 align-middle">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" 
                                    viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="md:w-9 md:h-9 h-8 w-8">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 01-1.043 3.296 3.745 3.745 0 01-3.296 1.043A3.745 3.745 0 0112 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 01-3.296-1.043 3.745 3.745 0 01-1.043-3.296A3.745 3.745 0 013 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 011.043-3.296 3.746 3.746 0 013.296-1.043A3.746 3.746 0 0112 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 013.296 1.043 3.746 3.746 0 011.043 3.296A3.745 3.745 0 0121 12z" />
                                </svg>
                            </div>
                            @endif
                        </h1>

                        @isset($data->sinode_id)
                        <h2 id="information-heading" class="sr-only">Sinode</h2>
                        <p class="mt-2 text-sm text-gray-500" data-aos="fade-left" data-aos-delay="250">{{ 'Sinode : '.$data->sinode->name_abbr }}</p>
                        @endif

                        <h2 id="information-heading" class="sr-only">Data views</h2>
                        <p class="mt-2 text-sm text-gray-500" data-aos="fade-left" data-aos-delay="250">{{ $data->viewed.' views' }}</p>
                    
                        <div class="flex flex-row gap-2 mt-2" data-aos="fade-left" data-aos-delay="250">
                            @if($data->instagram) <x-socialmedia.instagram-icon :url="$data->instagram"></x-socialmedia.instagram-icon> @endif
                        </div>
                    </div>
                </div>

                <div class="border-t border-gray-200 mt-5 pt-5" data-aos="fade-left" data-aos-delay="250">
                    <h3 class="text-md font-medium text-gray-900 mb-4">Services</h3>
                    <div class="prose prose-sm mt-1 text-gray-500 grid md:grid-cols-3 grid-cols-2 gap-2">
                        @foreach(explode(';', (str_replace('', '', $data->services))) as $item)
                            <x-text.list-box-with-icon :text="'Ibadah '.trim($item)"></x-text.list-box-with-icon>
                        @endforeach
                    </div>
                </div>

                <div class="border-t border-gray-200 mt-5 pt-5" data-aos="fade-left" data-aos-delay="250">
                    <h3 class="text-md font-medium text-gray-900 mb-4">Address</h3>
                    <div class="prose prose-sm text-gray-500">
                        {{ $data->address }}
                    </div>
                </div>

                <div class="mt-4 grid gap-x-2 gap-y-4 grid-cols-2" data-aos="fade-up" data-aos-delay="250">
                    <x-button.default 
                        :baseColor="'indigo-600'" 
                        :hoverColor="'indigo-900'" 
                        :textColor="'white'"
                        :text="'Direction'"
                        :href="'https://maps.google.com/?q='.$data->name">
                    </x-button.default>
                    @if($data->phone)
                    <x-button.default 
                        :baseColor="'green-600'" 
                        :hoverColor="'green-900'" 
                        :textColor="'white'"
                        :text="'Call'"
                        :href="'tel:'.trim(str_replace(['(', ')', ' '], '', $data->phone))">
                    </x-button.default>
                    @endif
                </div>
            </div>
        </div>

        <div class="lg:grid lg:grid-cols-7 lg:grid-rows-1 lg:gap-x-8 lg:gap-y-10 xl:gap-x-16 mt-5 pt-5 border-t border-gray-200">
            <!-- Product image -->
            <div class="lg:col-span-4 lg:row-end-1" data-aos="fade-right" data-aos-delay="150">
                
            </div>

            <!-- Product details -->
            <div class="mx-auto mt-4 max-w-2xl sm:mt-16 lg:col-span-3 lg:row-span-2 lg:row-end-2 lg:mt-0 lg:max-w-none">
                <div data-aos="fade-left" data-aos-delay="250">
                    <h3 class="text-md font-medium text-gray-900">Data Detail</h3>
                    <p class="mt-4 mb-4 text-sm text-gray-500">
                        Posted : {{ $data->created_at->format('d M Y h:i:s') }} <br/> 
                        Updated : {{ $data->updated_at->format('d M Y h:i:s') }} <br/>
                        <br/>
                        If this data is belongs to you or you have any question or request, please do not hesitate to contact us.
                    </p>
                    <x-button.default 
                        :baseColor="'indigo-50'" 
                        :hoverColor="'indigo-100'" 
                        :textColor="'indigo-700'"
                        :text="'Suggest Data Edit'">
                    </x-button.default>
                </div>

                <div class="border-t border-gray-200 mt-5 pt-5" data-aos="fade-left" data-aos-delay="250">
                    <h3 class="text-sm font-medium text-gray-900">Share</h3>
                    <ul role="list" class="mt-4 flex items-center space-x-6">
                        <li>
                            <a href="#" class="flex h-6 w-6 items-center justify-center text-gray-400 hover:text-gray-500">
                                <span class="sr-only">Share on Facebook</span>
                                <svg class="h-5 w-5" fill="currentColor" viewBox="0 0 20 20" aria-hidden="true">
                                <path fill-rule="evenodd" d="M20 10c0-5.523-4.477-10-10-10S0 4.477 0 10c0 4.991 3.657 9.128 8.438 9.878v-6.987h-2.54V10h2.54V7.797c0-2.506 1.492-3.89 3.777-3.89 1.094 0 2.238.195 2.238.195v2.46h-1.26c-1.243 0-1.63.771-1.63 1.562V10h2.773l-.443 2.89h-2.33v6.988C16.343 19.128 20 14.991 20 10z" clip-rule="evenodd"></path>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="flex h-6 w-6 items-center justify-center text-gray-400 hover:text-gray-500">
                                <span class="sr-only">Share on Instagram</span>
                                <svg class="h-6 w-6" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true">
                                <path fill-rule="evenodd" d="M12.315 2c2.43 0 2.784.013 3.808.06 1.064.049 1.791.218 2.427.465a4.902 4.902 0 011.772 1.153 4.902 4.902 0 011.153 1.772c.247.636.416 1.363.465 2.427.048 1.067.06 1.407.06 4.123v.08c0 2.643-.012 2.987-.06 4.043-.049 1.064-.218 1.791-.465 2.427a4.902 4.902 0 01-1.153 1.772 4.902 4.902 0 01-1.772 1.153c-.636.247-1.363.416-2.427.465-1.067.048-1.407.06-4.123.06h-.08c-2.643 0-2.987-.012-4.043-.06-1.064-.049-1.791-.218-2.427-.465a4.902 4.902 0 01-1.772-1.153 4.902 4.902 0 01-1.153-1.772c-.247-.636-.416-1.363-.465-2.427-.047-1.024-.06-1.379-.06-3.808v-.63c0-2.43.013-2.784.06-3.808.049-1.064.218-1.791.465-2.427a4.902 4.902 0 011.153-1.772A4.902 4.902 0 015.45 2.525c.636-.247 1.363-.416 2.427-.465C8.901 2.013 9.256 2 11.685 2h.63zm-.081 1.802h-.468c-2.456 0-2.784.011-3.807.058-.975.045-1.504.207-1.857.344-.467.182-.8.398-1.15.748-.35.35-.566.683-.748 1.15-.137.353-.3.882-.344 1.857-.047 1.023-.058 1.351-.058 3.807v.468c0 2.456.011 2.784.058 3.807.045.975.207 1.504.344 1.857.182.466.399.8.748 1.15.35.35.683.566 1.15.748.353.137.882.3 1.857.344 1.054.048 1.37.058 4.041.058h.08c2.597 0 2.917-.01 3.96-.058.976-.045 1.505-.207 1.858-.344.466-.182.8-.398 1.15-.748.35-.35.566-.683.748-1.15.137-.353.3-.882.344-1.857.048-1.055.058-1.37.058-4.041v-.08c0-2.597-.01-2.917-.058-3.96-.045-.976-.207-1.505-.344-1.858a3.097 3.097 0 00-.748-1.15 3.098 3.098 0 00-1.15-.748c-.353-.137-.882-.3-1.857-.344-1.023-.047-1.351-.058-3.807-.058zM12 6.865a5.135 5.135 0 110 10.27 5.135 5.135 0 010-10.27zm0 1.802a3.333 3.333 0 100 6.666 3.333 3.333 0 000-6.666zm5.338-3.205a1.2 1.2 0 110 2.4 1.2 1.2 0 010-2.4z" clip-rule="evenodd"></path>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="flex h-6 w-6 items-center justify-center text-gray-400 hover:text-gray-500">
                                <span class="sr-only">Share on Twitter</span>
                                <svg class="h-5 w-5" fill="currentColor" viewBox="0 0 20 20" aria-hidden="true">
                                <path d="M6.29 18.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0020 3.92a8.19 8.19 0 01-2.357.646 4.118 4.118 0 001.804-2.27 8.224 8.224 0 01-2.605.996 4.107 4.107 0 00-6.993 3.743 11.65 11.65 0 01-8.457-4.287 4.106 4.106 0 001.27 5.477A4.073 4.073 0 01.8 7.713v.052a4.105 4.105 0 003.292 4.022 4.095 4.095 0 01-1.853.07 4.108 4.108 0 003.834 2.85A8.233 8.233 0 010 16.407a11.616 11.616 0 006.29 1.84"></path>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>