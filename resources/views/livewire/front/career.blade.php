<!-- This example requires Tailwind CSS v3.0+ -->
<div class="bg-gray-900">
    <x-header.small-blank-header></x-header.small-blank-header>
    <div class="relative overflow-hidden pt-32 pb-96 lg:pt-40">
        <div><img class="absolute bottom-0 left-1/2 w-[1440px] max-w-none -translate-x-1/2" src="https://tailwindui.com/img/component-images/grid-blur-purple-on-black.jpg" alt=""></div>
        <div class="relative mx-auto max-w-7xl px-6 text-center lg:px-8">
            <div class="mx-auto max-w-2xl lg:max-w-4xl" data-aos="fade-right" data-aos-delay="150">
                <h2 class="text-lg font-semibold leading-8 text-white">Career</h2>
                <p class="mt-2 text-4xl font-bold tracking-tight text-white">Join our team, <br class="hidden sm:inline lg:hidden">wherever you are</p>
                <p class="mt-6 text-lg leading-8 text-white/60">Together, we help people around the world by supporting their Sunday needs</p>
            </div>
        </div>
    </div>
    <div class="flow-root bg-white pb-32 lg:pb-40">
        <div class="relative -mt-80">
            <div class="relative z-10 mx-auto max-w-7xl px-6 lg:px-8" data-aos="fade-right" data-aos-delay="200">
                <div class="mx-auto grid max-w-md grid-cols-1 gap-8 lg:max-w-4xl lg:grid-cols-2 lg:gap-8">
                    <x-pricing.card :title="'Data Entry'" :price="'$80'" 
                        :description="'Input, update, and validate the integrity of the church database.'"
                        :button="'Get Started Today'">
                        <x-pricing.point :point="'Part-time job'"></x-pricing.point>
                        <x-pricing.point :point="'20 data entry / day'"></x-pricing.point>
                    </x-pricing.card>
                    <x-pricing.card :title="'Content Writer'" :price="'$100'" 
                        :description="'Write contents in English about spiritual journey, christianity, or church.'"
                        :button="'Get Started Today'">
                        <x-pricing.point :point="'Part-time job'"></x-pricing.point>
                        <x-pricing.point :point="'2 post / week'"></x-pricing.point>
                    </x-pricing.card>
                </div>
            </div>
        </div>
        <div class="relative mx-auto mt-8 max-w-7xl px-6 lg:px-8">
            <div class="mx-auto max-w-md lg:max-w-4xl" data-aos="fade-right" data-aos-delay="500">
                <div class="flex flex-col gap-6 rounded-3xl p-8 ring-1 ring-gray-900/10 sm:p-10 lg:flex-row lg:items-center lg:gap-8">
                    <div class="lg:min-w-0 lg:flex-1">
                        <h3 class="text-lg font-semibold leading-8 tracking-tight text-primary">Disclaimer</h3>
                        <div class="mt-2 text-base leading-7 text-gray-600">The amount is not fixed and can be different based on the member's performance. This clause will be discussed in the recruitment process and described in the agreement.</div>
                    </div>
                    <div>
                        <a href="https://api.whatsapp.com/send?phone=6281221120660&text=Hello%2C%20My%20Sunday%20Notes%21%20I%20want%20to%20know%20more%20about%20part-time%20job%20shown%20in%20your%20website." 
                            class="inline-block rounded-lg bg-indigo-50 px-4 py-2.5 text-center text-sm font-semibold leading-5 text-primary hover:bg-indigo-100">Contact us for detail <span aria-hidden="true">&rarr;</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>