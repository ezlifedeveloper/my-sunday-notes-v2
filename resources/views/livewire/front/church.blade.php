<div class="mb-16">
    <x-header.small-blank-header></x-header.small-blank-header>
    <div class="bg-gray-900 pt-10 sm:pt-16 overflow-hidden lg:pt-8 pb-4 relative">
        <div><img class="absolute bottom-0 left-1/2 w-[1440px] max-w-none -translate-x-1/2" src="https://tailwindui.com/img/component-images/grid-blur-purple-on-black.jpg" alt=""></div>
        <div class="relative mx-auto max-w-7xl px-8">
            <div class="mx-auto max-w-full">
                <div class="lg:pt-8">
                    <h1 class="mt-4 text-4xl font-bold tracking-tight text-white sm:mt-5 sm:text-6xl lg:mt-6 xl:text-6xl">
                        <span class="block">Find churches in one click</span>
                    </h1>
                    {{-- <p class="mt-3 text-base text-gray-300 sm:mt-5 sm:text-xl lg:text-lg xl:text-xl">Our very own database will help you with the personal detail of the church, combined with the powerful google maps and our note to write sermon or preaches in your app.</p> --}}
                    <div class="mt-5 sm:mt-6">
                        <form action="#" class="w-full">
                            <div class="sm:flex">
                                <div class="min-w-0 flex-1">
                                    <label for="churchName" class="sr-only">Church Name</label>
                                    <input type="text" wire:model="search" placeholder="Enter church name" class="block w-full rounded-md border-0 px-4 py-3 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-300 focus:ring-offset-2 focus:ring-offset-gray-900">
                                </div>
                                <div class="mt-3 sm:mt-0 sm:ml-3">
                                    <button type="submit" class="block w-full rounded-md bg-primary py-3 px-4 font-medium text-white shadow hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-indigo-300 focus:ring-offset-2 focus:ring-offset-gray-900">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="mb-4">
                <h3 class="text-lg font-medium leading-6 text-gray-900">Last 30 days</h3>
                <dl class="grid grid-cols-1 gap-4 sm:grid-cols-3">
                    <x-header.info-box :title="'Total Church Data'" :value="number_format($total, 0, '.', ',')"></x-header.info-box>
                    <x-header.info-box :title="'Total Sinode Data'" :value="number_format($sinode, 0, '.', ',')"></x-header.info-box>
                    <x-header.info-box :title="'Most Viewed Church'" :value="$viewed"></x-header.info-box>
                </dl>
            </div>
        </div>
    </div>
    <div class="bg-white pt-8">
        <x-text.announcement :text="'You can help us by suggesting your church!'" :url="''" :cta="'Suggest a church'"></x-text.announcement>
        <div class="mx-auto max-w-7xl py-8 px-8">
            <div class="sm:flex sm:items-baseline sm:justify-between">
                <div class="flex items-center">
                    <h2 class="text-2xl font-bold tracking-tight text-gray-900 mr-2">
                        {{ count($data) }} data found
                    </h2>
                    <div class="group relative inline-block">                  
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9.879 7.519c1.171-1.025 3.071-1.025 4.242 0 1.172 1.025 1.172 2.687 0 3.712-.203.179-.43.326-.67.442-.745.361-1.45.999-1.45 1.827v.75M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9 5.25h.008v.008H12v-.008z" />
                        </svg>
                        <div class="absolute left-full top-1/2 z-20 ml-3 -translate-y-1/2 whitespace-nowrap rounded bg-black py-[6px] px-4 text-sm font-semibold text-white opacity-0 group-hover:opacity-100">
                            <span class="absolute left-[-3px] top-1/2 -z-10 h-2 w-2 -translate-y-1/2 rotate-45 rounded-sm bg-black"></span>
                            We limit the results to 12 datas
                        </div>
                    </div>
                </div>
                <a href="#" class="hidden text-sm font-semibold text-indigo-600 hover:text-indigo-500 sm:block">
                    Sign Up to Continue
                    <span aria-hidden="true"> &rarr;</span>
                </a>
            </div>
      
            <div class="mt-6 grid grid-cols-1 gap-y-10 sm:grid-cols-4 sm:gap-y-0 sm:gap-6 lg:gap-8">
                @foreach($data as $d)
                <x-church.card-vertical :data="$d"></x-church.card-vertical>
                @endforeach
            </div>
        
            <div class="mt-6 sm:hidden">
                <a href="#" class="block text-sm font-semibold text-indigo-600 hover:text-indigo-500">
                    Sign Up to Continue
                    <span aria-hidden="true"> &rarr;</span>
                </a>
            </div>
        </div>
    </div>
</div>
