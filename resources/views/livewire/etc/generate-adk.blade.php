<div>
    <div class="p-8 gap-5 mt-5 xs:gap-5">
        <div class="h-full p-4 mb-4 bg-white rounded-lg shadow xs:span-col-1">
            <div class="grid grid-cols-1 gap-x-4">
                <x-form.file :title="'File JSON'" :model="'file'" :multiple="false" :type="'*'"></x-form.file>

                <button wire:click.prevent="load"
                    class="inline-flex justify-center px-4 py-2 ml-3 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Generate d_item
                </button>
            </div>

            @if($successLoad)
            <div class="mt-4 flex flex-col gap-2">
                <span class="text-black font-bold">List file of {{$timestamp}}</span>

                <span class="text-blue-500"><a href="{{$csvPath}}" download>File CSV</a> </span>
                <span class="text-blue-500"><a href="{{$xmlPath}}" download>File XML</a> </span>
            </div>
            @endif
        </div>
    </div>
</div>


