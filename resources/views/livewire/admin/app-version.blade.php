<div class="mt-5">
    <dl class="grid h-full grid-cols-2 gap-5 xs:grid-cols-1 xl:grid-cols-4">
        <x-admin.info-bar-2 :color="'blue-500'" :title="$title" :text="$total">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
        </x-admin.info-bar-2>
        <x-admin.info-bar-2 :color="'blue-800'" :title="'Website'" :text="$totalWeb">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
        </x-admin.info-bar-2>
        <x-admin.info-bar-2 :color="'black'" :title="'Mobile'" :text="$totalMobile">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
        </x-admin.info-bar-2>
    </dl>

    @if($openForm)
    <x-admin.modal-base-add>
        <div class="mb-4">
            <h3 class="text-lg font-medium text-gray-900">
                @if($dataId) Update @else Add @endif {{ $title }} Data
            </h3>
        </div>
        <div>
            <x-form.text :title="'Platform'" :model="'inputPlatform'" :required="true"></x-form.text>
            <x-form.text :title="'Version'" :model="'inputVersion'" :required="true"></x-form.text>
            <x-form.text :title="'Desc'" :model="'inputDesc'" :required="true"></x-form.text>
            <x-form.date :title="'Date'" :model="'inputDate'" :required="true"></x-form.date>
        </div>
    </x-admin.modal-base-add>
    @endif

    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <x-table :headerTitle="['No', 'Date', 'Version', 'Platform', 'Desc', 'Last Update', '']" :data="$data">
            @section('header')
            <x-button.create></x-button.create>
            @endsection

            @section('content')
            @foreach($data as $d)
            <x-table.tr :striped="$loop->index%2" :isSoftDelete="false">
                <x-table.numbering :data="$data" :loop="$loop"/>
                <x-table.td :align="'center'">{{ $d->date }}</x-table.td>
                <x-table.td :align="'center'">{{ $d->version }}</x-table.td>
                <x-table.td :align="'center'">{{ $d->platform }}</x-table.td>
                <x-table.td :align="'center'">{{ $d->desc }}</x-table.td>
                <x-table.creator :data="$d"/>

                <x-table.td :class="'gap-4 font-medium text-right'">
                    <x-button.edit :id="$d->id"></x-button.edit>
                    <x-button.delete :id="$d->id"></x-button.delete>
                </x-table.td>
            </x-table.tr>
            @endforeach
            @endsection
        </x-table>
    </div>
</div>
