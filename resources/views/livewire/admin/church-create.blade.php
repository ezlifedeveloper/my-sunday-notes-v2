@push('styles')
<style>
    /* Always set the map height explicitly to define the size of the div
    * element that contains the map. */
    #map {
      height: 400px;
      width: 100%;
    }

    #infowindow-content .title {
      font-weight: bold;
    }

    #infowindow-content {
      display: none;
    }

    #map #infowindow-content {
      display: inline;
    }
</style>
@endpush

<div>
    <div class="gap-5 mt-5 xs:gap-5">
        <div class="h-full p-4 mb-4 bg-white rounded-lg shadow xs:span-col-1">
            <div class="mb-4">
                <h3 class="text-lg font-medium text-gray-900">
                    @if($dataId) Update @else Add @endif {{ $title }} Data
                </h3>
            </div>
            <div class="grid grid-cols-1 md:grid-cols-2 gap-x-4">
                <x-form.text :title="'Church Name'" :model="'inputName'" :required="true" :placeholder="'Masukkan nama gereja'"></x-form.text>
                <x-form.select :title="'Pilih Sinode'" :model="'inputSinode'" :required="true">
                    <option value="">Pilih Sinode</option>
                    @foreach ($sinode as $item)
                        <option value="{{$item->id}}" @if($inputSinode == $item->id) selected @endif>{{$item->name}}</option>
                    @endforeach
                </x-form.select>
                @error('inputSinode') <span class="text-red-500">{{ $message }}</span> @enderror
                <x-form.text :title="'Phone'" :model="'inputPhone'" :required="true" :placeholder="'+628123456789'"></x-form.text>
                <x-form.text :title="'Service (semicolon separator)'" :model="'inputService'" :required="true" :placeholder="'07:00; 09:00; 11:00'"></x-form.text>
                <div class="grid md:grid-cols-3 md:col-span-2 md:gap-x-4">
                    <x-form.text :title="'Website'" :model="'inputWebsite'" :placeholder="'Masukkan url website gereja'"></x-form.text>
                    <x-form.text :title="'Play Store'" :model="'inputPlayStore'" :placeholder="'Masukkan url aplikasi gereja di playstore'"></x-form.text>
                    <x-form.text :title="'App Store'" :model="'inputAppStore'" :placeholder="'Masukkan url aplikasi gereja di app store'"></x-form.text>
                </div>
                <div class="grid md:grid-cols-3 md:col-span-2 md:gap-x-4">
                    <x-form.text :title="'Instagram'" :model="'inputInstagram'" :placeholder="'Masukkan url Instagram gereja'"></x-form.text>
                    <x-form.text :title="'Twitter'" :model="'inputTwitter'" :placeholder="'Masukkan url Twitter gereja'"></x-form.text>
                    <x-form.text :title="'Facebook'" :model="'inputFacebook'" :placeholder="'Masukkan url Facebook gereja'"></x-form.text>
                </div>
                <div class="mb-4 md:col-span-2">
                    <x-form.text :title="'Select Location'" :model="'inputLocation'" :class="'input-index'" :name="'pac-input'" :id="'pac-input'" :placeholder="'Enter location name'"></x-form.text>
                    <div id="map"></div>
                </div>
                <div class="grid md:grid-cols-4 md:col-span-2 md:gap-x-4">
                    <x-form.text :title="'Address'" :model="'inputAddress'" :class="'map md:col-span-2'" :required="true" :placeholder="'Masukkan alamat gereja'" :id="'address'"></x-form.text>
                    <x-form.text :title="'Latitude'" :model="'inputLatitude'" :class="'map'" :required="true" :placeholder="'Masukkan latitude gereja'" :id="'latitude'"></x-form.text>
                    <x-form.text :title="'Longitude'" :model="'inputLongitude'" :class="'map'" :required="true" :placeholder="'Masukkan longitude gereja'" :id="'longitude'"></x-form.text>
                </div>
                <x-form.file :title="'Image'" :model="'photo'" :multiple="false" :type="'image/*'"></x-form.file>
                @if ($photo)
                    Preview:
                    <img class="h-40" src="{{ !is_string($photo) ? $photo->temporaryUrl() : $photo }}">
                @endif
            </div>

            <div class="pt-5">
                <div class="flex justify-end">
                    <a type="button" href="{{ route('back.church') }}"
                        class="px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Cancel
                    </a>
                    <button wire:click.prevent="storeData"
                        class="inline-flex justify-center px-4 py-2 ml-3 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Save
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxQfd0X4Ai62z50ckEq-vnxtKjW7S1rcE&libraries=places&callback=initAllMap" async defer></script>
<script>
    function setAddress(idLatitude, idLongitude, idAlamat, id_latitude, id_longitude, id_alamat, lat, lng, address) {
        @this.set('inputLatitude', lat);
        @this.set('inputLongitude', lng);
        @this.set('inputAddress', address);
    }

    function initMap(idMap, idInfoWindow, idPacInput, idPlaceIcon, idPlaceName, idPlaceAddress, idLatitude, idLongitude, idAlamat, _idLintang, _idBujur, _idAlamat) {
    var map = new google.maps.Map(document.getElementById(idMap), {
        center: {lat: -7.283991, lng: 112.759684},
        zoom: 13
    });

    var input = document.getElementById(idPacInput);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById(idInfoWindow);
    infowindow.setContent(infowindowContent);
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
        }

        if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
        } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var geocoder = new google.maps.Geocoder;
        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();
        var newPos = new google.maps.LatLng(lat, lng);
        var address = '';

        geocoder.geocode({'location': newPos}, function (result, status) {
        if (status === 'OK') {
            if (result[0]) {
                address = [result[0].formatted_address];
                infowindow.open(map, marker);
                setAddress(idLatitude, idLongitude, idAlamat, _idLintang, _idBujur, _idAlamat, lat, lng, address);
            }
        }
        });
    });

    google.maps.event.addListener(map, 'click', function (event) {
        var lat = event.latLng.lat();
        var lng = event.latLng.lng();
        var newPos = new google.maps.LatLng(lat, lng);
        var geocoder = new google.maps.Geocoder;

        marker.setVisible(false);
        map.setCenter(newPos);
        map.setZoom(17);
        marker.setPosition(newPos);
        marker.setVisible(true);
        var address = '';

        geocoder.geocode({'location': newPos}, function (result, status) {
        if (status === 'OK') {
            if (result[0]) {
            console.log(lat);
            address = result[0].formatted_address;
            setAddress(idLatitude, idLongitude, idAlamat, _idLintang, _idBujur, _idAlamat, lat, lng, address);
            }
        }
        });
    });
    }

    function initAllMap() {
        initMap('map', 'infowindow-content', 'pac-input', 'place-icon', 'place-name', 'place-address', 'latitude', 'longitude', 'address', 'latitude', 'longitude', 'alamat');
    }
</script>
@endpush