<div class="mt-5">
    <dl class="grid h-full grid-cols-2 gap-5 xs:grid-cols-1 xl:grid-cols-4">
        <x-admin.info-bar-2 :color="'blue-500'" :title="$title" :text="$total">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
        </x-admin.info-bar-2>
    </dl>

    @if($openForm)
    <x-admin.modal-base-add>
        <div class="mb-4">
            <h3 class="text-lg font-medium text-gray-900">
                @if($editData) Update @else Add @endif User Data
            </h3>
        </div>
        <div>
            <x-form.text :title="'Nama User'" :model="'inputName'" :required="true"></x-form.text>
            <x-form.text :title="'Username'" :model="'inputUsername'" :required="true"></x-form.text>
            <x-form.text :title="'Email'" :model="'inputEmail'" :required="true"></x-form.text>
            
            <div class="mb-4">
                <x-form.select :title="'Pilih Role'" :model="'inputRole'" :required="true">
                    <option value="">Pilih Role</option>
                    @foreach ($role as $item)
                        <option value="{{$item->id}}" @if($inputRole == $item->id) selected @endif>{{$item->name}}</option>
                    @endforeach
                </x-form.select>
                @error('inputRole') <span class="text-red-500">{{ $message }}</span> @enderror
            </div>
            <div class="mb-4">
                <label for="password" class="block text-sm font-medium text-gray-700">
                    Password
                </label>
                <div class="mt-1">
                    <input type="password" name="password" id="password" wire:model.defer='inputPassword'
                        class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-green-500 focus:border-green-500 sm:text-sm">
                </div>
            </div>

        </div>
    </x-admin.modal-base-add>
    @endif

    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <x-table :headerTitle="['No', 'User', 'Role', 'Email', 'Point', 'Last Update', '']" :data="$data">
            @section('header')
            <x-button.create></x-button.create>
            @endsection

            @section('content')
            @foreach($data as $d)
            <x-table.tr :striped="$loop->index%2" :isSoftDelete="true" :deleted="$d->trashed()">
                <x-table.numbering :data="$data" :loop="$loop"/>
                <x-table.td :align="'left'">{{ $d->name }}<br/>{{ 'U : '.$d->username }}</x-table.td>
                <x-table.td :align="'center'">
                    <div class="flex justify-center gap-2">
                    @if($d->role_id == 1)
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-6 h-6">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M9.594 3.94c.09-.542.56-.94 1.11-.94h2.593c.55 0 1.02.398 1.11.94l.213 1.281c.063.374.313.686.645.87.074.04.147.083.22.127.324.196.72.257 1.075.124l1.217-.456a1.125 1.125 0 011.37.49l1.296 2.247a1.125 1.125 0 01-.26 1.431l-1.003.827c-.293.24-.438.613-.431.992a6.759 6.759 0 010 .255c-.007.378.138.75.43.99l1.005.828c.424.35.534.954.26 1.43l-1.298 2.247a1.125 1.125 0 01-1.369.491l-1.217-.456c-.355-.133-.75-.072-1.076.124a6.57 6.57 0 01-.22.128c-.331.183-.581.495-.644.869l-.213 1.28c-.09.543-.56.941-1.11.941h-2.594c-.55 0-1.02-.398-1.11-.94l-.213-1.281c-.062-.374-.312-.686-.644-.87a6.52 6.52 0 01-.22-.127c-.325-.196-.72-.257-1.076-.124l-1.217.456a1.125 1.125 0 01-1.369-.49l-1.297-2.247a1.125 1.125 0 01.26-1.431l1.004-.827c.292-.24.437-.613.43-.992a6.932 6.932 0 010-.255c.007-.378-.138-.75-.43-.99l-1.004-.828a1.125 1.125 0 01-.26-1.43l1.297-2.247a1.125 1.125 0 011.37-.491l1.216.456c.356.133.751.072 1.076-.124.072-.044.146-.087.22-.128.332-.183.582-.495.644-.869l.214-1.281z" />
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                    @elseif($d->role_id == 3)
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-6 h-6">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125" />
                    </svg>                                     
                    @else
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-6 h-6">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
                    </svg>                      
                    @endif
                    @isset($d->password)
                    <img class="w-6 h-6" src="{{ asset('logo.png') }}">
                    @else
                    <img class="w-6 h-6" src="{{ asset('google_icon.png') }}">
                    @endif
                    </div>
                </x-table.td>
                <x-table.td :align="'center'">{{ $d->email ?? '-' }}</x-table.td>
                <x-table.td :align="'center'">{{ $d->point }}</x-table.td>
                <x-table.creator :data="$d"/>

                <x-table.td :class="'gap-4 font-medium text-right'">
                    @if(!$d->trashed())
                    <x-button.edit :id="$d->id"></x-button.edit>
                    @else
                    <x-button.restore :id="$d->id"></x-button.restore>
                    @endif
                    <x-button.delete :id="$d->id"></x-button.delete>
                </x-table.td>
            </x-table.tr>
            @endforeach
            @endsection
        </x-table>
    </div>
</div>
