
<div class="mt-5">
    <dl class="grid h-full grid-cols-2 gap-5 xs:grid-cols-1 xl:grid-cols-4">
        <x-admin.info-bar-2 :color="'blue-500'" :title="$title" :text="$total">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
        </x-admin.info-bar-2>
    </dl>

    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <x-table :headerTitle="['No', 'Image', 'Name', 'Address', 'Last Update', '']" :data="$data">
            @section('header')
            <a type="button" href="{{ route('back.church-create') }}"
                class="inline-flex items-center px-3 py-2 text-sm font-medium leading-4 text-white bg-blue-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                <!-- Heroicon name: solid/mail -->
                <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4" />
                </svg>
                Create
            </a>
            @endsection

            @section('content')
            @foreach($data as $d)
            <x-table.tr :striped="$loop->index%2" :isSoftDelete="true" :deleted="$d->trashed()">
                <x-table.numbering :data="$data" :loop="$loop"/>
                <x-table.td :align="'center'">
                    <img src="{{ $d->image }}" class="h-24">    
                </x-table.td>
                <x-table.td :align="'left'">{{ $d->name }}</x-table.td>
                <x-table.td :align="'left'">{{ $d->address }}</x-table.td>
                <x-table.creator :data="$d"/>

                <x-table.td :class="'gap-4 font-medium text-right'">
                    @if(!$d->trashed())
                    <button wire:click="setVerified({{$d->id}}, {{!$d->is_verified}})" 
                        class="text-{{$d->is_verified ? 'red' : 'green'}}-600 hover:text-{{$d->is_verified ? 'red' : 'green'}}-900">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            @if($d->is_verified)
                            <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />                              
                            @else
                            <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M4.5 12.75l6 6 9-13.5" />
                            @endif
                        </svg>
                    </button>
                    <a type="button" href="{{ url('office/church/edit/'.$d->id) }}" class="text-indigo-600 hover:text-indigo-900">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                        </svg>
                    </a>
                    @else
                    <x-button.restore :id="$d->id"></x-button.restore>
                    @endif
                    @if(Helper::isAllowed(1))
                    <x-button.delete :id="$d->id"></x-button.delete>
                    @endif
                </x-table.td>
            </x-table.tr>
            @endforeach
            @endsection
        </x-table>
    </div>
</div>
