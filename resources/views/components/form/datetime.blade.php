@props(['title', 'model', 'useHeader' => true, 'required', 'disabled' => false])
<div class="sm:col-span-1 @if($useHeader) mb-4 @endif">
    @if($useHeader)
    <label class="block text-sm font-medium text-gray-700">
        {{ $title }}
    </label>
    @endif
    <div class="@if($useHeader) mt-1 @endif">
        <input type="datetime-local" wire:model='{{ $model }}' {{ $required ? 'required' : '' }} {{
            $disabled ? 'disabled' : '' }}
            class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
    </div>
</div>