@props(['title' => null, 'model' => 'inputPassword', 'required' => false, 'placeholder', 'defer' => true])
<div class="mb-4">
    <label for="password" class="block text-sm font-medium text-gray-700">
        Password
    </label>
    <div class="mt-1">
        <input type="password" placeholder="{{$placeholder}}"
            @if($defer) wire:model.defer='{{ $model }}' @else wire:model='{{ $model }}' @endif {{ $required ? 'required' : '' }}
            class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
    </div>
</div>