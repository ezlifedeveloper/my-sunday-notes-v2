@props(['title', 'model', 'useHeader' => true, 'required' => false, 'multiple' => false, 'disabled' => false, 'type'])
<div class="sm:col-span-1 @if($useHeader) mb-4 @endif">
    @if($useHeader)
    <label class="block text-sm font-medium text-gray-700">
        {{$title}}
    </label>
    @endif
    <div class="relative mt-1">
        <input type="file" wire:model='{{ $model }}' {{ $required ? 'required' : '' }} {{ $multiple ? 'multiple' : '' }}
            {{ $disabled ? 'disabled' : '' }} accept="{{ $type }}"
            class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
        <div wire:loading wire:target="{{ $model }}">Uploading...</div>
    </div>
</div>
