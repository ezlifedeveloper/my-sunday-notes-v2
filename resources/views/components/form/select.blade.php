@props(['title', 'model', 'required', 'disabled' => false, 'useHeader' => true, 'defer' => true])
<div class="sm:col-span-1 w-content @if($title) mb-4 @endif">
    @if($useHeader)
    <label class="block text-sm font-medium text-gray-700">
        {{$title}}
    </label>
    @endif
    <div class="@if($title) mt-1 @endif">
        <select @if($defer) wire:model.defer='{{ $model }}' @else wire:model='{{ $model }}' @endif {{ $required ? 'required' : '' }} {{ $disabled ? 'disabled' : '' }}
            class="block w-full px-3 py-2 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            {{ $slot }}
        </select>
    </div>
</div>
