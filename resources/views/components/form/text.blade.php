@props(['title' => null, 'model', 'required' => false, 'disabled' => false, 'defer' => true, 'placeholder' => "", 'class' => "", 'name' => "", 'id' => ""])
<div class="sm:col-span-1 @if($title) mb-4 @endif {{$class}}">
    @if($title)
    <label class="block text-sm font-medium text-gray-700">
        {{ $title }}
    </label>
    @endif
    <div class="@if($title) mt-1 @endif">
        <input type="text" @if($defer) wire:model.defer='{{ $model }}' @else wire:model='{{ $model }}' @endif {{ $required ? 'required' : '' }} {{ $disabled ? 'disabled' : '' }}
            placeholder="{{ $placeholder }}" @isset($name) name="{{$name}}" @endif @isset($id) id="{{$id}}" @endif
            class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
    </div>
</div>
