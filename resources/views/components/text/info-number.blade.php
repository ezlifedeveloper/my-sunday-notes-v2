@props(['number', 'title', 'pos'])
<div class="flex flex-col border-b border-gray-100 p-4 text-center sm:border-0 @if($pos!='l') sm:border-l @endif  @if($pos!='r') sm:border-r @endif group hover:bg-primary">
    <dt class="order-2 mt-2 text-md font-medium leading-6 text-gray-500 group-hover:text-white">{{ $title }}</dt>
    <dd class="order-1 text-3xl font-bold tracking-tight text-primary group-hover:text-white">{{ $number }}</dd>
</div>