@props(['title', 'detail', 'fade' => 'right', 'delay' => "750"])
<div data-aos="fade-{{$fade}}" data-aos-delay="{{$delay}}">
    <dt class="text-base font-semibold leading-7 text-gray-900">
    <div class="mb-4 flex h-10 w-10 items-center justify-center rounded-lg bg-primary">
        <!-- Heroicon name: outline/trash -->
        <svg class="h-6 w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
            {{ $slot }}
        </svg>
    </div>
    {{ $title }}
    </dt>
    <dd class="mt-1 text-base leading-7 text-gray-600">{{ $detail }}</dd>
</div>