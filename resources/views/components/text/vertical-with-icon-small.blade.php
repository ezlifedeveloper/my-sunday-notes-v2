@props(['title', 'detail' => 'Ullam laboriosam est voluptatem maxime ut mollitia commodi. Et dignissimos suscipit perspiciatis.', 'fade' => 'right', 'delay' => "750"])
<div class="pt-6" data-aos="fade-{{$fade}}" data-aos-delay="{{$delay}}">
    <div class="flow-root rounded-lg bg-gray-100 px-6 pb-8">
        <div class="-mt-6">
            <div>
                <span class="inline-flex items-center justify-center rounded-md bg-gradient-to-r from-teal-500 to-cyan-600 p-3 shadow-lg">
                    <svg class="h-6 w-6 text-white" x-description="Heroicon name: outline/shield-check" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                        {{ $slot }}
                    </svg>
                </span>
            </div>
            <h3 class="mt-8 text-lg font-medium tracking-tight text-gray-900">{{ $title }}</h3>
            <p class="mt-5 text-base text-gray-500">{{ $detail }}</p>
        </div>
    </div>
</div>