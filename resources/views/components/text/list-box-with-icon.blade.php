@props(['text'])
<div class="flex w-full items-center text-center justify-center rounded-md border border-transparent 
    bg-gray-600 py-3 px-3 text-base font-medium text-white hover:bg-black
    focus:outline-none focus:ring-2 focus:ring-blue-300 focus:ring-offset-2 focus:ring-offset-gray-50">
    {{-- <svg class="h-6 w-6 text-white mr-3" x-description="Heroicon name: outline/shield-check" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
        <path stroke-linecap="round" stroke-linejoin="round" d="M13.19 8.688a4.5 4.5 0 011.242 7.244l-4.5 4.5a4.5 4.5 0 01-6.364-6.364l1.757-1.757m13.35-.622l1.757-1.757a4.5 4.5 0 00-6.364-6.364l-4.5 4.5a4.5 4.5 0 001.242 7.244" />
    </svg> --}}
    <span type="span">
        {{ $text }}
    </span>
</div>