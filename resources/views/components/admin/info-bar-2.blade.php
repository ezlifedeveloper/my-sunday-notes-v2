@props(['color', 'title', 'text'])

<div class="relative p-4 overflow-hidden bg-{{$color}} rounded-lg shadow ">
    <dt>
        <div class="absolute p-3 bg-white rounded-md">
            <!-- Heroicon name: outline/mail-open -->
            <svg class="w-6 h-6 text-{{$color}}" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                stroke="currentColor" aria-hidden="true">
                {{ $slot }}
            </svg>
        </div>
        <p class="ml-16 text-sm font-medium text-white truncate">{{ $title }}</p>
    </dt>
    <dd class="flex items-baseline ml-16">
        <p class="text-lg font-semibold text-white">
            {{ $text }}
        </p>
    </dd>
</div>
