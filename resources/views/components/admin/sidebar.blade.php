@props(['title'])

<div x-data="{ open: false, sidebarOpen: false }" @keydown.window.escape="open = false">
    <div x-show="open" class="fixed inset-0 z-40 flex md:hidden"
        x-description="Off-canvas menu for mobile, show/hide based on off-canvas menu state." x-ref="dialog"
        aria-modal="true" style="display: none;">

        <div x-show="open" x-transition:enter="transition-opacity ease-linear duration-300"
            x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100"
            x-transition:leave="transition-opacity ease-linear duration-300" x-transition:leave-start="opacity-100"
            x-transition:leave-end="opacity-0"
            x-description="Off-canvas menu overlay, show/hide based on off-canvas menu state."
            class="fixed inset-0 bg-gray-600 bg-opacity-75" @click="open = false" aria-hidden="true"
            style="display: none;">
        </div>

        <div x-show="open" x-transition:enter="transition ease-in-out duration-300 transform"
            x-transition:enter-start="-translate-x-full" x-transition:enter-end="translate-x-0"
            x-transition:leave="transition ease-in-out duration-300 transform" x-transition:leave-start="translate-x-0"
            x-transition:leave-end="-translate-x-full"
            x-description="Off-canvas menu, show/hide based on off-canvas menu state."
            class="relative flex flex-col flex-1 w-full max-w-xs bg-white" style="display: none;">

            <div x-show="open" x-transition:enter="ease-in-out duration-300" x-transition:enter-start="opacity-0"
                x-transition:enter-end="opacity-100" x-transition:leave="ease-in-out duration-300"
                x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
                x-description="Close button, show/hide based on off-canvas menu state."
                class="absolute top-0 right-0 pt-2 -mr-12" style="display: none;">
                <button type="button"
                    class="flex items-center justify-center w-10 h-10 ml-1 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                    @click="open = false">
                    <span class="sr-only">Close sidebar</span>
                    <svg class="w-6 h-6 text-gray-900" x-description="Heroicon name: outline/x"
                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                        aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12">
                        </path>
                    </svg>
                </button>
            </div>

            <div class="flex-1 h-0 pt-5 pb-4 overflow-y-auto">
                <div class="flex items-center flex-shrink-0 px-4">
                    <a href="{{ route('home') }}">
                        <img class="w-auto h-8" src="{{ asset('logo.png') }}" alt="{{ env('APP_NAME') }}">
                    </a>
                </div>
                <nav class="px-2 mt-5">
                    <x-admin.sidebar-list :title="$title"/>
                </nav>
            </div>
            <div class="flex flex-shrink-0 p-4 border-t border-indigo-800">
                <a href="{{ route('logout') }}" class="flex-shrink-0 block group">
                    <div class="flex items-center">
                        <div>
                            <x-admin.sidebar-user-photo></x-admin.sidebar-user-photo>
                        </div>
                        <div class="ml-3">
                            <p class="text-base font-medium text-gray-900">
                                {{ Auth::user()->name }}
                            </p>
                            <p class="text-base font-medium text-gray-900">
                                {{ Auth::user()->role->name }}
                            </p>
                            <p class="text-sm font-medium text-indigo-200 group-hover:text-gray-900">
                                Logout
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="flex-shrink-0 w-14" aria-hidden="true">
            <!-- Force sidebar to shrink to fit close icon -->
        </div>
    </div>

    <!-- Static sidebar for desktop -->
    <div class="hidden transition-all duration-300 md:flex md:w-64 md:flex-col md:fixed md:inset-y-0"
        :class="{ '-ml-64': !sidebarOpen }">
        <!-- Sidebar component, swap this element with another sidebar if you like -->
        <div class="flex flex-col flex-1 min-h-0 bg-white border-r border-gray-200">
            <div class="flex flex-col flex-1 pt-5 pb-4 overflow-y-auto">
                <div class="flex items-center flex-shrink-0 px-4">
                    <a href="{{ route('home') }}">
                        <img class="w-auto h-8" src="{{ asset('logo.png') }}" alt="{{ env('APP_NAME') }}">
                    </a>
                </div>
                <nav class="flex-1 px-2 mt-5">
                    <x-admin.sidebar-list :title="$title"/>
                </nav>
            </div>
            <div class="flex flex-shrink-0 p-4 border-t border-indigo-800">
                <a href="{{ route('logout') }}" class="flex-shrink-0 block w-full group">
                    <div class="flex items-center">
                        <div>
                            <x-admin.sidebar-user-photo></x-admin.sidebar-user-photo>
                        </div>
                        <div class="ml-3">
                            <p class="text-sm font-medium text-gray-700 group-hover:text-gray-900">
                                {{ Auth::user()->name }}
                            </p>
                            <p class="text-xs font-medium text-gray-700 group-hover:text-gray-900">
                                {{ Auth::user()->role->name }}
                            </p>
                            <p class="text-xs font-medium text-gray-500 group-hover:text-gray-700">
                                Logout
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="flex flex-row items-center flex-1 transition-all duration-300 md:pl-64 -ml-64 bg-gray-100 " :class="{ '-ml-64': !sidebarOpen }">
        <div class="sticky top-0 z-10 hidden pt-1 pl-1 md:block sm:pl-3 sm:pt-3">
            <button type="button"
                class="-ml-0.5 -mt-0.5 h-12 w-12 inline-flex items-center justify-center rounded-md text-gray-500 hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500"
                @click="sidebarOpen = !sidebarOpen">
                <span class="sr-only">Open sidebar</span>
                <svg class="w-6 h-6" x-description="Heroicon name: outline/menu" xmlns="http://www.w3.org/2000/svg"
                    fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16">
                    </path>
                </svg>
            </button>
        </div>
    </div>
    <div class="flex flex-row flex-1 items-center md:pl-64 bg-gray-100 ">
        <div class="sticky top-0 z-10 pt-1 pl-1 md:hidden sm:pl-3 sm:pt-3">
            <button type="button"
                class="-ml-0.5 -mt-0.5 h-12 w-12 inline-flex items-center justify-center rounded-md text-gray-500 hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500"
                @click="open = true">
                <span class="sr-only">Open sidebar</span>
                <svg class="w-6 h-6" x-description="Heroicon name: outline/menu" xmlns="http://www.w3.org/2000/svg"
                    fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16">
                    </path>
                </svg>
            </button>
        </div>
    </div>

    <div class="flex flex-col flex-1 transition-all duration-300 md:pl-64" :class="{ 'md:-ml-64': !sidebarOpen }">
        {{$slot}}
    </div>
</div>
