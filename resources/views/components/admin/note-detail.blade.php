@props(['data' => null])
<div class="fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
    <div class="flex items-center justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100"
            x-transition:leave="ease-in duration-200" x-transition:leave-start="opacity-100" x-transition:enter-end="opacity-0"
            class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
  
        <!-- This element is to trick the browser into centering the modal contents. -->
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
        <div x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95" x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
            x-transition:leave="ease-in duration-200" x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100" x-transition:enter-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            class="inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-xl md:max-w-3xl sm:w-full sm:p-6">
            <table class="min-w-full">
                <tbody>
                    <tr>
                        <x-table.td>Date</x-table.td>
                        <x-table.td :break="false">: {{ $data->date->format('d M Y') }}</x-table.td>
                    </tr>
                    <tr>
                        <x-table.td>Title</x-table.td>
                        <x-table.td>: {{ $data->title }}</x-table.td>
                    </tr>
                    <tr>
                        <x-table.td>Location</x-table.td>
                        <x-table.td>: {{ $data->location }}</x-table.td>
                    </tr>
                    <tr>
                        <x-table.td>Event</x-table.td>
                        <x-table.td>: {{ $data->event }}</x-table.td>
                    </tr>
                    <tr>
                        <x-table.td>Speaker</x-table.td>
                        <x-table.td>: {{ $data->speaker }}</x-table.td>
                    </tr>
                    <tr>
                        <x-table.td>Verse</x-table.td>
                        <x-table.td>: {{ $data->verse }}</x-table.td>
                    </tr>
                    <tr>
                        <x-table.td :colspan="2">{!! nl2br($data->note) !!}</x-table.td>
                    </tr>
                    <!-- More plans... -->
                </tbody>
            </table>
            
            <div class="pt-5">
                <div class="flex justify-end">
                    <button wire:click="viewDetail"
                        class="px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Close
                    </button>
                </div>
            </div>            
        </div>
    </div>
</div>