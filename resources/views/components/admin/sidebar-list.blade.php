@props(['title'])

@if(Helper::isAllowed(1))
<x-admin.sidebar-item :url="route('back.dashboard')" :name="'Dashboard'"
    :active="($title=='Dashboard' ? true : false)">
    <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" 
        d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />                   
</x-admin.sidebar-item>
@endif

<x-admin.sidebar-item :url="route('back.profile')" :name="'Profile'"
    :active="($title=='Profile' ? true : false)">
    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z" />
</x-admin.sidebar-item>

<x-admin.sidebar-header :name="'Note'"></x-admin.sidebar-header>

<x-admin.sidebar-item :url="route('back.note')" :name="'Note'" :active="($title=='Note' ? true : false)">
    <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M12 6.042A8.967 8.967 0 006 3.75c-1.052 0-2.062.18-3 .512v14.25A8.987 8.987 0 016 18c2.305 0 4.408.867 6 2.292m0-14.25a8.966 8.966 0 016-2.292c1.052 0 2.062.18 3 .512v14.25A8.987 8.987 0 0018 18a8.967 8.967 0 00-6 2.292m0-14.25v14.25" />
</x-admin.sidebar-item>

@if(Helper::isAllowed(1, 3))
<x-admin.sidebar-header :name="'Church'"></x-admin.sidebar-header>

@if(Helper::isAllowed(1))
<x-admin.sidebar-item :url="route('back.church-dashboard')" :name="'Dashboard'" :active="($title=='Church Dashboard' ? true : false)">
    <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M3 13.125C3 12.504 3.504 12 4.125 12h2.25c.621 0 1.125.504 1.125 1.125v6.75C7.5 20.496 6.996 21 6.375 21h-2.25A1.125 1.125 0 013 19.875v-6.75zM9.75 8.625c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125v11.25c0 .621-.504 1.125-1.125 1.125h-2.25a1.125 1.125 0 01-1.125-1.125V8.625zM16.5 4.125c0-.621.504-1.125 1.125-1.125h2.25C20.496 3 21 3.504 21 4.125v15.75c0 .621-.504 1.125-1.125 1.125h-2.25a1.125 1.125 0 01-1.125-1.125V4.125z" />
</x-admin.sidebar-item>
<x-admin.sidebar-item :url="route('back.church-collaborator')" :name="'Collaborator'" :active="($title=='Church Collaborator' ? true : false)">
    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
    d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
</x-admin.sidebar-item>
<x-admin.sidebar-item :url="route('back.church-request')" :name="'Request'" :active="($title=='Church Request' ? true : false)">
    <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M9.879 7.519c1.171-1.025 3.071-1.025 4.242 0 1.172 1.025 1.172 2.687 0 3.712-.203.179-.43.326-.67.442-.745.361-1.45.999-1.45 1.827v.75M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9 5.25h.008v.008H12v-.008z" />
</x-admin.sidebar-item>
@endif
<x-admin.sidebar-item :url="route('back.sinode')" :name="'Sinode'" :active="($title=='Sinode' ? true : false)">
    <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M20.25 6.375c0 2.278-3.694 4.125-8.25 4.125S3.75 8.653 3.75 6.375m16.5 0c0-2.278-3.694-4.125-8.25-4.125S3.75 4.097 3.75 6.375m16.5 0v11.25c0 2.278-3.694 4.125-8.25 4.125s-8.25-1.847-8.25-4.125V6.375m16.5 0v3.75m-16.5-3.75v3.75m16.5 0v3.75C20.25 16.153 16.556 18 12 18s-8.25-1.847-8.25-4.125v-3.75m16.5 0c0 2.278-3.694 4.125-8.25 4.125s-8.25-1.847-8.25-4.125" />
</x-admin.sidebar-item>
<x-admin.sidebar-item :url="route('back.church')" :name="'Church'"
    :active="($title=='Church' ? true : false)">
    <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" 
        d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />                   
</x-admin.sidebar-item>
@endif
  
@if(Helper::isAllowed(1))
<x-admin.sidebar-header :name="'App'"></x-admin.sidebar-header>

<x-admin.sidebar-item :url="route('back.app.review')" :name="'Review'" :active="($title=='App Review' ? true : false)">
    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7.5 8.25h9m-9 3H12m-9.75 1.51c0 1.6 1.123 2.994 2.707 3.227 1.129.166 2.27.293 3.423.379.35.026.67.21.865.501L12 21l2.755-4.133a1.14 1.14 0 01.865-.501 48.172 48.172 0 003.423-.379c1.584-.233 2.707-1.626 2.707-3.228V6.741c0-1.602-1.123-2.995-2.707-3.228A48.394 48.394 0 0012 3c-2.392 0-4.744.175-7.043.513C3.373 3.746 2.25 5.14 2.25 6.741v6.018z" />
</x-admin.sidebar-item>
<x-admin.sidebar-item :url="route('back.app.version')" :name="'Version'" :active="($title=='App Version' ? true : false)">
    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 21a9.004 9.004 0 008.716-6.747M12 21a9.004 9.004 0 01-8.716-6.747M12 21c2.485 0 4.5-4.03 4.5-9S14.485 3 12 3m0 18c-2.485 0-4.5-4.03-4.5-9S9.515 3 12 3m0 0a8.997 8.997 0 017.843 4.582M12 3a8.997 8.997 0 00-7.843 4.582m15.686 0A11.953 11.953 0 0112 10.5c-2.998 0-5.74-1.1-7.843-2.918m15.686 0A8.959 8.959 0 0121 12c0 .778-.099 1.533-.284 2.253m0 0A17.919 17.919 0 0112 16.5c-3.162 0-6.133-.815-8.716-2.247m0 0A9.015 9.015 0 013 12c0-1.605.42-3.113 1.157-4.418" />
</x-admin.sidebar-item>  
@endif
  
@if(Helper::isAllowed(1))
<x-admin.sidebar-header :name="'User'"></x-admin.sidebar-header>

<x-admin.sidebar-item :url="route('back.role')" :name="'Role'" :active="($title=='User Role' ? true : false)">
    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
        d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
</x-admin.sidebar-item>
<x-admin.sidebar-item :url="route('back.user')" :name="'Data'" :active="($title=='User' ? true : false)">
    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
        d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
</x-admin.sidebar-item>
@endif