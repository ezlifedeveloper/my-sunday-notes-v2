@props(['name'])

<a class="text-black bg-white group flex items-center px-2 py-2 text-sm font-medium mt-4
    rounded-md">
    {{ $name }}
</a>
