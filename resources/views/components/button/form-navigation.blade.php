@props(['back', 'exit'])
<div class="flex flex-row justify-between mb-4 w-full gap-1 px-8">
    <div class="flex gap-2">
        <x-button.back :route="$back"></x-button.back>
        <x-button.exit :route="$exit"></x-button.exit>
    </div>
    <div class="flex gap-2">
        <x-button.save-only></x-button.save-only>
        <x-button.save-continue></x-button.save-continue>
    </div>
</div>