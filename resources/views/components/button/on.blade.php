@props(['id'])
<button wire:click='switchStatus({{$id}}, true)'
    onclick="confirm('Are you sure to activate?') || event.stopImmediatePropagation()"
    class="text-green-600 hover:text-green-900">
    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M5.636 5.636a9 9 0 1012.728 0M12 3v9" />
    </svg>      
</button>
