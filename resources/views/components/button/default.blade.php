@props(['baseColor', 'hoverColor', 'textColor', 'text', 'href' => ''])
<a type="button" href="{{ $href }}"
    class="flex w-full items-center justify-center rounded-md border border-transparent 
        bg-{{ $baseColor }} py-3 px-8 text-base font-medium text-{{ $textColor }} hover:bg-{{ $hoverColor }}
        focus:outline-none focus:ring-2 focus:ring-{{ $baseColor }} focus:ring-offset-2 focus:ring-offset-gray-50">
    {{ $text }}
</a>