@props(['link', 'title', 'active' => false])
@if($active)
<a href="{{ $link }}" class="py-2 px-4 text-base font-medium text-gray-900 bg-white rounded-lg">{{ $title }}</a>
@else
<a href="{{ $link }}" class="py-2 px-4 text-base font-medium text-white hover:text-gray-900 hover:bg-white hover:rounded-lg">{{ $title }}</a>
@endif