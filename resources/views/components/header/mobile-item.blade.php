@props(['link', 'title'])
<a href="{{ $link }}" class="block rounded-md px-3 py-2 text-base font-medium text-primary hover:bg-gray-50">{{ $title }}</a>