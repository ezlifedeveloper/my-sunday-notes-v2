@props(['title'])
<header class="relative" x-data="{ open: false, focus: true }" @keydown.escape="onEscape" @close-popover-group.window="onClosePopoverGroup">
    <div class="bg-gray-900 pt-2">
        <nav class="relative mx-auto flex max-w-7xl items-center justify-between px-4 sm:px-6" aria-label="Global">
            <div class="flex flex-1 items-center">
                <div class="flex w-full items-center justify-between md:w-auto">
                    <a href="{{ route('home') }}">
                        <span class="sr-only">Your Company</span>
                        <img class="h-8 w-8" src="{{ asset('logo.png') }}" alt="My Sunday Notes">
                    </a>
                    <div class="-mr-2 flex items-center md:hidden">
                        <button type="button" class="focus-ring-inset inline-flex items-center justify-center rounded-md bg-gray-900 p-2 text-gray-400 hover:bg-gray-800 focus:outline-none focus:ring-2 focus:ring-white" @click="open = !open" @mousedown="if (open) $event.preventDefault()" aria-expanded="false" :aria-expanded="open.toString()">
                            <span class="sr-only">Open main menu</span>
                            <svg class="h-6 w-6" x-description="Heroicon name: outline/bars-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"></path>
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="hidden space-x-2 md:ml-10 md:flex">
                    <x-header.web-item :link="route('front.church')" :title="'Church'" :active="$title=='Church'"></x-header.web-item>
                    <x-header.web-item :link="route('front.note')" :title="'Notes'" :active="$title=='Note'"></x-header.web-item>
                    {{-- <x-header.web-item :link="route('front.post')" :title="'Post'" :active="$title=='Post'"></x-header.web-item> --}}
                    {{-- <x-header.web-item :link="route('front.api')" :title="'API'" :active="$title=='API'"></x-header.web-item> --}}
                    <x-header.web-item :link="route('front.career')" :title="'Career'" :active="$title=='Career'"></x-header.web-item>
                    {{-- <x-header.web-item :link="route('front.profile')" :title="'Our Profile'" :active="$title=='Profile'"></x-header.web-item> --}}
                    {{-- <x-header.web-item :link="route('front.profile')" :title="'Our Profile'" :active="$title=='Profile'"></x-header.web-item> --}}
                </div>
            </div>
            <div class="hidden md:flex md:items-center md:space-x-6">
                {{-- <x-header.web-item :link="'#'" :title="'Login'"></x-header.web-item> --}}
                <a href="{{ route('login') }}" class="inline-flex items-center rounded-md border border-transparent bg-gray-600 px-4 py-2 text-base font-medium text-white hover:bg-gray-700">
                    @guest Login @else Office @endif
                </a>
            </div>
        </nav>
    </div>
    
    {{-- Mobile View --}}
    <div x-show="open" x-transition:enter="duration-150 ease-out" x-transition:enter-start="opacity-0 scale-95" x-transition:enter-end="opacity-100 scale-100" x-transition:leave="duration-100 ease-in" x-transition:leave-start="opacity-100 scale-100" x-transition:leave-end="opacity-0 scale-95" x-description="Mobile menu, show/hide based on menu open state." class="absolute inset-x-0 top-0 z-10 origin-top transform p-2 transition md:hidden" x-ref="panel" @click.away="open = false" style="display: none;">
        <div class="overflow-hidden rounded-lg bg-white shadow-md ring-1 ring-black ring-opacity-5">
            <div class="flex items-center justify-between px-5 pt-4">
                <div>
                    <a href="{{ route('home') }}">
                        <img class="h-8 w-auto" src="{{ asset('logo.png') }}" alt="My Sunday Notes">
                    </a>
                </div>
                <div class="-mr-2">
                    <button type="button" class="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-600" @click="open = !open">
                        <span class="sr-only">Close menu</span>
                        <svg class="h-6 w-6" x-description="Heroicon name: outline/x-mark" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12"></path>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="pt-5 pb-6">
                <div class="space-y-1 px-2">
                    <x-header.mobile-item :link="route('front.church')" :title="'Church'"></x-header.mobile-item>
                    <x-header.mobile-item :link="route('front.note')" :title="'Notes'"></x-header.mobile-item>
                    {{-- <x-header.mobile-item :link="route('front.post')" :title="'Post'"></x-header.mobile-item> --}}
                    {{-- <x-header.mobile-item :link="route('front.api')" :title="'API'"></x-header.mobile-item> --}}
                    <x-header.mobile-item :link="route('front.career')" :title="'Career'"></x-header.mobile-item>
                    {{-- <x-header.mobile-item :link="route('front.profile')" :title="'Our Profile'"></x-header.mobile-item> --}}
                </div>
                <div class="mt-6 px-5">
                    {{-- <a href="#" class="block w-full rounded-md bg-indigo-600 py-3 px-4 text-center font-medium text-white shadow hover:bg-indigo-700">Start free trial</a> --}}
                </div>
                <div class="mt-6 px-5">
                    <p class="text-center text-base font-medium text-gray-500">Existing customer? <a href="#" class="text-primary hover:underline">Login</a></p>
                </div>
            </div>
        </div>
    </div>    
</header>