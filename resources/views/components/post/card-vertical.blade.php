@props(['image' => 'https://images.unsplash.com/photo-1492724441997-5dc865305da7?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1679&amp;q=80', 
    'category', 'title', 'description'])
<div class="flex flex-col overflow-hidden rounded-lg shadow-lg">
    <div class="flex-shrink-0">
    <img class="h-48 w-full object-cover" src="{{ $image }}" alt="">
    </div>
    <div class="flex flex-1 flex-col justify-between bg-white p-6">
        <div class="flex-1">
            <p class="text-sm font-medium text-cyan-600">
            <a href="#" class="hover:underline">{{ $category }}</a>
            </p>
            <a href="#" class="mt-2 block">
            <p class="text-xl font-semibold text-gray-900">{{ $title }}</p>
            <p class="mt-3 text-base text-gray-500">{{ $description }}</p>
            </a>
        </div>
        <div class="mt-6 flex items-center">
            <div class="flex-shrink-0">
            <a href="#">
                <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1487412720507-e7ab37603c6f?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=2&amp;w=256&amp;h=256&amp;q=80" alt="Daniela Metz">
            </a>
            </div>
            <div class="ml-3">
            <p class="text-sm font-medium text-gray-900">
                <a href="#" class="hover:underline">Daniela Metz</a>
            </p>
            <div class="flex space-x-1 text-sm text-gray-500">
                <time datetime="2020-02-12">Feb 12, 2020</time>
                <span aria-hidden="true">·</span>
                <span>11 min read</span>
            </div>
            </div>
        </div>
    </div>
</div>