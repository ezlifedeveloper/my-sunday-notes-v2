@props(['title', 'category', 'seeMore' => false,
    'description' => 'Phasellus lorem quam molestie id quisque diam aenean nulla in. Accumsan in quis quis nunc, ullamcorper malesuada. Eleifend condimentum id viverra nulla.'])

<div class="relative mb-8">
    @isset($title)
    <div class="mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8">
        <h2 class="text-lg font-semibold text-cyan-600">{{ $category }}</h2>
        <p class="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">{{ $title }}</p>
        <p class="mx-auto mt-5 max-w-prose text-xl text-gray-500">{{ $description }}</p>
    </div>
    @endif
    <div class="mx-auto mt-12 grid max-w-lg gap-8 sm:max-w-2xl lg:gap-12 lg:max-w-7xl lg:grid-cols-3 lg:px-8 md:grid-cols-2">
        {{ $slot }}
    </div>
    @if($seeMore)
    <div class="mx-auto mt-8 max-w-md px-4 sm:max-w-lg sm:px-6 lg:max-w-7xl lg:grid-cols-3 lg:px-8">
        <a href="{{ route('front.post') }}" class="text-right hidden text-md font-semibold text-indigo-600 hover:text-indigo-500 sm:block">
            See more posts
            <span aria-hidden="true"> &rarr;</span>
        </a>
    </div>
    @endif
</div>