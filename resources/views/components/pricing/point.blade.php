@props(['point'])
<li class="flex items-start">
    <div class="flex-shrink-0">
        <!-- Heroicon name: outline/check -->
        <svg class="h-6 w-6 text-primary" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
        <path stroke-linecap="round" stroke-linejoin="round" d="M4.5 12.75l6 6 9-13.5" />
        </svg>
    </div>
    <p class="ml-3 text-sm leading-6 text-black font-medium">{{ $point }}</p>
</li>