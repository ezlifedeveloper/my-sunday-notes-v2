@props(['title', 'price', 'description', 'button', 'link'])
<div class="flex flex-col rounded-3xl bg-white shadow-xl ring-1 ring-black/10">
    <div class="p-8 sm:p-10">
        <h3 class="text-lg font-semibold leading-8 tracking-tight text-primary" id="tier-hobby" data-aos="fade-right" data-aos-delay="200">{{ $title }}</h3>
        <div class="mt-4 flex items-baseline text-5xl font-bold tracking-tight text-gray-900" data-aos="fade-right" data-aos-delay="500">
            {{ $price }}
            <span class="text-lg font-semibold leading-8 tracking-normal text-gray-500">/mo</span>
        </div>
        <p class="mt-6 text-base leading-7 text-gray-600" data-aos="fade-right" data-aos-delay="700">{{ $description }}</p>
    </div>
    <div class="flex flex-1 flex-col p-2">
        <div class="flex flex-1 flex-col justify-between rounded-2xl bg-gray-100 p-6 sm:p-8">
            <ul role="list" class="space-y-6" data-aos="fade-right" data-aos-delay="500">
                {{ $slot }}
            </ul>
            @isset($link)
            <div class="mt-8">
                <a href="{{ $link }}" class="inline-block w-full rounded-lg bg-primary px-4 py-2.5 text-center text-sm font-semibold leading-5 text-white shadow-md hover:bg-indigo-700" aria-describedby="tier-hobby">
                    {{ $button }}
                </a>
            </div>
            @endisset
        </div>
    </div>
</div>