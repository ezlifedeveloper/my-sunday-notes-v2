@props(['data'])
<div class="group relative">
    <a href="{{ url('church/'.$data->slug) }}">
        <div class="min-h-80 aspect-w-1 aspect-h-1 w-full overflow-hidden rounded-md bg-gray-200 group-hover:opacity-75 lg:h-40 relative">
            <img src="{{ $data->image }}" alt="{{ $data->name }}" class="object-fill">
        </div>
        <div class="mt-4 flex justify-between">
            <div class="flex-1">
                <h3 class="text-sm text-gray-700">
                    <span aria-hidden="true" class="absolute inset-0"></span>
                    {{ $data->name }}
                </h3>
                <p class="mt-1 text-sm text-gray-500">{{ $data->services }}</p>
            </div>
            <div class="text-right">
                <p class="text-sm font-medium text-gray-900">{{ $data->distance ?? '0.95 km' }}</p>
                <p class="text-sm font-medium text-gray-500">{{ $data->viewed.' views' }}</p>
            </div>
        </div>
    </a>
</div>