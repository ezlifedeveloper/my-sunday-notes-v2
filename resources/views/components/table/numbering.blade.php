@props(['data', 'loop'])
<x-table.td :break="false" :align="'center'">{{ $data->perPage()*($data->currentPage()-1)+$loop->iteration}}</x-table.td>