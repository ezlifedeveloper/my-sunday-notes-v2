@props(['search' => true, 'pagination' => true])
<div class="flex flex-row justify-between h-full mt-5">
    <div class="flex flex-row mb-4 w-full gap-1">
        {{ $slot }}
    </div>

    <div class="flex flex-row gap-1 mb-4 min-w-content">
        @if($pagination) <x-table.perpage></x-table.perpage> @endif
        @if($search) <x-table.search></x-table.search> @endif
    </div>
</div>
