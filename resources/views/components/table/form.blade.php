<div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8 sm:rounded-lg">
    <div class="flex flex-row justify-between h-full mt-5">
        @yield('header')
    </div>
    
    <div class="overflow-x border-b border-gray-200 shadow text-black">
        <table class="table-auto min-w-full divide-y divide-gray-200 border-collapse border border-slate-300">
            <tbody class="bg-white divide-y divide-gray-200">
                @yield('content')
            </tbody>
        </table>
    </div>
</div>
