@props(['question' => ''])

<div class="overflow-x border-b border-gray-200 shadow mx-8 mt-6">
    {{$question}}
    <table class="table-auto min-w-full divide-y divide-gray-200 border-collapse border border-slate-300">
        <tbody class="bg-white divide-y divide-gray-200">
            {{$slot}}
        </tbody>
    </table>
</div>