@props(['data', 'pagination' => true])
<div class="mt-4 text-xs">
    @if($pagination && $data->hasPages())
    {{ $data->links() }}
    @endif
</div>
