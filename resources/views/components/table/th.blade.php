@props(['break' => true, 'class' => '' , 'align' => 'left', 'border' => false, 'colspan' => 1, 'rowspan' => 1])
<th colspan="{{$colspan}}" rowspan="{{$rowspan}}" class="text-sm p-3 @if($break) break-words @else whitespace-nowrap @endif {{$class}} text-{{$align}} border border-black">
    {{ $slot }}
</th>