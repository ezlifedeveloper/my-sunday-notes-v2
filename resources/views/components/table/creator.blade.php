@props(['data'])
<x-table.td :align="'center'">
    {{ $data->updator ? $data->updator->name : ($data->creator ? $data->creator->name : '-') }}<br/>
    {{ $data->updated_at }}
</x-table.td>