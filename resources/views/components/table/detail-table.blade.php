@props(['title', 'align' => 'center'])
<div class="grid grid-cols-1 md:grid-cols-4 gap-1 mb-4">
    <div class="col-span-1 flex justify-between font-bold">
        @if($title)
        <span>{{ $title }}</span>
        <span class="invisible md:visible">:</span>
        @endif
    </div>
    <div class="col-span-1 md:col-span-3">
        {{ $slot }}
    </div>
</div>