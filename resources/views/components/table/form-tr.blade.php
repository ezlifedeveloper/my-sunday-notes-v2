@props(['id', 'number', 'title', 'type', 'options' => [], 'isAdmin' => false, 'trashed' => false])
<tr>
    <x-table.td :border="true" :align="'center'">{{ $number }}</x-table.td>
    <x-table.td :border="true" :align="'left'">{{ $title }}</x-table.td>
    <x-table.td :border="true" :align="'left'">
        <x-input :type="$type" :options="$options" :title="Helper::cleanStr($title)" :disabled="$isAdmin"></x-input>
    </x-table.td>
    @if($isAdmin)
    <x-table.td :border="true" :align="'center'">
        @if(!$trashed)
            <x-button.edit :id="$id"></x-button.edit>
        @else
            <x-button.restore :id="$id"></x-button.restore>
        @endif 
        <x-button.delete :id="$id"></x-button.delete>
    </x-table.td>
    @endif
</tr>