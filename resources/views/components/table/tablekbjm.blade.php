@props(['jmlrowspan','throwspan','jmlcolspan','thcolspan','headerTitle','data','pagination','search' => false, ])

<div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8 sm:rounded-lg">
    <x-table.header :search="$search" :pagination="$pagination">
        @yield('header')
    </x-table.header>
    <div class="overflow-x border-b border-gray-200 shadow">
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-blue-800">
                <tr class="font-medium text-black text-md text-white text-center uppercase">
                    @foreach ($throwspan as $item)
                        <th rowspan="{{$jmlrowspan}}" scope="col" class="px-6 py-3 tracking-wider">
                            {{$item}}
                        </th>
                     @endforeach
                     @foreach ($thcolspan as $item)
                        <th colspan="{{$jmlcolspan}}" scope="col" class="px-6 py-3 tracking-wider">
                             {{$item}}
                         </th>
                    @endforeach
                    <th rowspan="2">ACTION</th>
                </tr>
                <tr class="font-medium text-black text-md text-white text-center uppercase">
                    @foreach ($headerTitle as $item)
                        <th scope="col" class="px-6 py-3 tracking-wider">
                            {{$item}}
                        </th>
                    @endforeach
                </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
                @if(count($data) > 0)
                    @yield('content')
                @else
                    @php
                        $c1 = count($throwspan);
                        $c2 = count($headerTitle);
                    @endphp
                    <tr>
                        <td colspan="{{$c1+$c2}}" class="px-6 py-4 text-sm text-center">
                            No data available
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
    {{-- <x-table.footer :data="$data" :pagination="$pagination"></x-table.footer> --}}
</div>