<select wire:model="perPage"
    class="form-control relative flex-auto min-w-content block w-20 px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">
    <option>All</option>
    <option>5</option>
    <option>10</option>
    <option>15</option>
    <option>25</option>
</select>
