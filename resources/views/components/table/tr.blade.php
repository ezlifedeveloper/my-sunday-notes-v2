@props(['isStriped' => true, 'striped' => false, 'isSoftDelete' => false, 'deleted' => false, 'loading' => false])
<tr @if($loading) wire:loading.class='text-gray-100' @endif class="
    @if($isSoftDelete && $deleted) text-gray-200 @endif
    @if($isStriped && $striped) bg-gray-100 @endif
">
    {{ $slot }}
</tr>
