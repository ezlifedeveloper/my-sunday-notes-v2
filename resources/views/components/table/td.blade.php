@props(['break' => true, 'class' => '' , 'align' => 'left', 'border' => false, 'colspan' => 1, 'rowspan' => 1])
<td :colspan="{{$colspan}}" :rowspan="{{$rowspan}}" class="p-3 @if($break) break-words @else whitespace-nowrap @endif {{$class}} text-{{$align}} @if($border) border border-black @endif">
    {{ $slot }}
</td>