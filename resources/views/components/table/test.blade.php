@props([
	'data' => [],
	'striped' => false,
	'actionText' => 'Action',
	'tableTextLinkLabel' => 'Link',
    'search' => false,
    'pagination' => true
])

<div 
    x-data="{
        data: {{ collect($data) }},
        isStriped: Boolean({{ $striped }}),
        ...content()
    }"
    x-cloak
    class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8 sm:rounded-lg"
>
    <x-table.header :search="$search" :pagination="$pagination">
        {{ $header }}
        <button @click="expand(data.length)" class="inline-flex items-center px-3 py-2 mr-3 text-sm font-medium leading-4 text-white bg-blue-500 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Expand All</button>
        <button @click="collapse()" class="inline-flex items-center px-3 py-2 mr-3 text-sm font-medium leading-4 text-white bg-blue-500 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Collapse All</button>
        
    </x-table.header>

	<div class="mb-5 overflow-x-auto bg-white rounded-lg shadow overflow-y-auto relative">
        <div class="grid grid-cols-12">
            <div class="px-6 py-3 text-xs font-medium tracking-wider text-center uppercase bg-blue-100 col-span-1 min-w-content">No</div>
            <div class="px-6 py-3 text-xs font-medium tracking-wider text-center uppercase bg-blue-100 col-span-2 min-w-content">Kode Satker</div>
            <div class="px-6 py-3 text-xs font-medium tracking-wider text-center uppercase bg-blue-100 col-span-2 min-w-content">Sts History</div>
            <div class="px-6 py-3 text-xs font-medium tracking-wider text-center uppercase bg-blue-100 col-span-1 min-w-content">Revisi Ke</div>
            <div class="px-6 py-3 text-xs font-medium tracking-wider text-center uppercase bg-blue-100 col-span-2 min-w-content">Pagu Belanja</div>
            <div class="px-6 py-3 text-xs font-medium tracking-wider text-center uppercase bg-blue-100 col-span-2 min-w-content">Data Anggaran</div>
            <div class="px-6 py-3 text-xs font-medium tracking-wider text-center uppercase bg-blue-100 col-span-2 min-w-content">Sync</div>
        </div>

        {{ $tableData }}
        {{-- {{dd($data)}} --}}
        
	</div>
</div>
<script>
    function content() {
        return {
            active: [],
            checkActive(index) {
                return this.active.includes(index)
            },
            setActive(index) {
                if(this.active.includes(index)){
                    deletedIndex = this.active.indexOf(index)
                    this.active.splice(deletedIndex, 1)
                } else {
                    this.active.push(index)
                }
            },
            collapse() {
                this.active = []
            },
            expand(total) {
                this.active = [...Array(total).keys()]
            },
            sync(kdsatker, kdstshistory = null) {
                $wire.sync(kdsatker, kdstshistory)
            },
        }
    }
</script>