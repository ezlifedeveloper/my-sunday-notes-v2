<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\UserRole;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        UserRole::truncate();
        UserRole::create([
            'name' => 'Superuser',
            'created_by' => 2,
            'updated_by' => 2,
        ]);
        UserRole::create([
            'name' => 'User',
            'created_by' => 2,
            'updated_by' => 2,
        ]);
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
