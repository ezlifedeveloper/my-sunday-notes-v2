<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constraint('users');
            $table->date('date')->nullable();
            $table->string('title')->nullable();
            $table->string('time', 5)->nullable();
            $table->string('location')->nullable();
            $table->string('event')->nullable();
            $table->string('speaker')->nullable();
            $table->string('verse')->nullable();
            $table->longText('note')->nullable();
            
            $table->foreignId('created_by')->constraint('users')->nullable();
            $table->foreignId('updated_by')->constraint('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
    }
};
