<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bible_lists', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('text');
            $table->string('language');
            $table->string('source');
            
            $table->foreignId('created_by')->constraint('users')->nullable();
            $table->foreignId('updated_by')->constraint('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bible_lists');
    }
};
