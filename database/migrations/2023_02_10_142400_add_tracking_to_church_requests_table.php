<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('church_requests', function (Blueprint $table) {
            $table->string('tracking')->default('issued');
            $table->string('type')->default('issued');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('church_requests', function (Blueprint $table) {
            $table->dropColumn('tracking');
            $table->dropColumn('type');
        });
    }
};
