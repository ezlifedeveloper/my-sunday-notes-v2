<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->date('birth_date')->nullable();
            $table->string('sex', 1)->nullable();
            $table->string('church')->nullable();
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('photo')->nullable();
            $table->foreignId('role_id')->constraint('user_roles');
            $table->tinyInteger('status')->default(1);

            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->string('api_token')->nullable();
            $table->longText('expo_token')->nullable();
            $table->boolean('google_auth')->default(false);

            $table->unsignedBigInteger('point')->default(0);

            $table->foreignId('created_by')->constraint('users')->nullable();
            $table->foreignId('updated_by')->constraint('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
