<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('churches', function (Blueprint $table) {
            $table->id();
            $table->foreignId('sinode_id')->constraint('sinodes')->nullable();
            $table->string('name');
            $table->string('address');
            $table->string('phone')->nullable();
            $table->string('services')->nullable();
            $table->string('lat');
            $table->string('lng');
            $table->string('image')->nullable();

            $table->string('website')->nullable();
            $table->string('android')->nullable();
            $table->string('ios')->nullable();
            $table->boolean('app_showed')->default(false);

            $table->unsignedBigInteger('viewed')->default(0);
            
            $table->foreignId('created_by')->constraint('users')->nullable();
            $table->foreignId('updated_by')->constraint('users')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('churches');
    }
};
