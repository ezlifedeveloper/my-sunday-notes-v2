<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('church_requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constraint('users');
            $table->foreignId('sinode_id')->constraint('sinodes')->nullable();
            $table->foreignId('church_id')->constraint('churches')->nullable();
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('services')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('image')->nullable();
            $table->unsignedTinyInteger('status')->default(0);

            $table->foreignId('updated_by')->constraint('users')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('church_requests');
    }
};
