<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sinodes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('name_abbr');
            $table->string('desc')->nullable();

            $table->foreignId('created_by')->constraint('users');
            $table->foreignId('updated_by')->constraint('users')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sinodes');
    }
};
