<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SyncUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync User Data from Old Server';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Start '.$this->description);
        Log::info('Starting data migration...');

        $deleteCurrent = User::truncate();
        $data = DB::connection('old_mysql')->table('users')->get();
        // $data = Note::all();
        foreach($data as $d) {
            // DB::connection('old_mysql')
            //     ->table('notes')
            //     ->insert([
            //         'date' => $d->date,
            //         'id_user' => $d->user_id,
            //         'time' => $d->time,
            //         'location' => $d->location,
            //         'event' => $d->event,
            //         'speaker' => $d->speaker,
            //         'verse' => $d->verse,
            //         'note' => $d->note,
            //         'title' => $d->title
            //     ]);
            $user = new User();
            $user->name = $d->name;
            $user->username = $d->username;
            $user->birth_date = $d->birth_date;
            $user->sex = $d->sex;
            $user->church = $d->church;
            $user->email = $d->email;
            $user->email_verified_at = $d->email_verified_at;
            $user->created_at = $d->created_at;
            $user->updated_at = $d->updated_at;
            $user->api_token = $d->api_token;
            $user->point = $d->point;
            $user->role_id = $d->role == 'admin' ? 1 : 2;
            $user->status = 1;
            $user->google_auth = isset($d->password) ? 0 : 1;
            $user->password = $d->password;
            $user->save();
        }

        Log::info($this->description.' finished');
        return Command::SUCCESS;
    }
}
