<?php

namespace App\Console\Commands;

use App\Models\Note;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SyncNote extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:note';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Note Data from Old Server';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Start '.$this->description);
        Log::info('Starting data migration...');

        $deleteCurrent = Note::truncate();
        $data = DB::connection('old_mysql')->table('notes')->get();
        // $data = Note::all();
        foreach($data as $d) {
            // DB::connection('old_mysql')
            //     ->table('notes')
            //     ->insert([
            //         'date' => $d->date,
            //         'id_user' => $d->user_id,
            //         'time' => $d->time,
            //         'location' => $d->location,
            //         'event' => $d->event,
            //         'speaker' => $d->speaker,
            //         'verse' => $d->verse,
            //         'note' => $d->note,
            //         'title' => $d->title
            //     ]);
            $note = new Note();
            $note->date = $d->date;
            $note->user_id = $d->id_user;
            $note->time = $d->time;
            $note->location = $d->location;
            $note->event = $d->event;
            $note->speaker = $d->speaker;
            $note->verse = $d->verse;
            $note->note = $d->note;
            $note->title = $d->title;
            $note->created_by = $d->id_user;
            $note->updated_by = $d->id_user;
            $note->deleted_at = (!isset($d->state) || !$d->state) ? null : $d->updated_at;
            $note->save();
        }

        Log::info($this->description.' finished');
        return Command::SUCCESS;
    }
}
