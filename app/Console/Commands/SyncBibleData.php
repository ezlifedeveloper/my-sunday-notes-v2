<?php

namespace App\Console\Commands;

use App\Models\Bible;
use App\Models\BibleList;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SyncBibleData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:bible';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Bible Data from Old Server';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Start '.$this->description);

        Log::info('Loading bible list...');
        $bl = [];
        $bibleList = BibleList::all();
        foreach($bibleList as $b) {
            $bl[$b->name] = $b->id;
        }
        Log::info('Bible list loaded...');
        Log::info('Starting data migration...');

        // $data = Bible::truncate();
        // $data = DB::connection('old_mysql')->table('bibles')->get();
        $data = Bible::with(['version'])->get();
        foreach($data as $d) {
            // DB::connection('old_mysql')
            //     ->table('bibles')
            //     ->insert([
            //         'book_num' => $d->book_num,
            //         'book' => $d->book,
            //         'chapter' => $d->chapter,
            //         'verse_number' => $d->verse_number,
            //         'verse_text' => $d->verse_text,
            //         'parallel' => $d->parallel,
            //         'passage' => $d->passage,
            //         'version_id' => $d->version->name,
            //         'version_text' => $d->version->text,
            //     ]);
                DB::connection('new_mysql')
                    ->table('bibles')
                    ->insert([
                        'book_num' => $d->book_num,
                        'book' => $d->book,
                        'chapter' => $d->chapter,
                        'verse_number' => $d->verse_number,
                        'verse_text' => $d->verse_text,
                        'parallel' => $d->parallel,
                        'passage' => $d->passage,
                        'version_id' => $d->version_id,
                    ]);
            // $bible = new Bible();
            // $bible->book_num = $d->book_num;
            // $bible->book = $d->book;
            // $bible->chapter = $d->chapter;
            // $bible->verse_number = $d->verse_number;
            // $bible->verse_text = $d->verse_text;
            // $bible->parallel = $d->parallel;
            // $bible->passage = $d->passage;
            // $bible->version_id = $bl[$d->version_id];
            // $bible->save();
        }

        Log::info($this->description.' finished');
        return Command::SUCCESS;
    }
}
