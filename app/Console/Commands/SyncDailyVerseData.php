<?php

namespace App\Console\Commands;

use App\Models\DailyVerse;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SyncDailyVerseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:dailyverse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Daily Verse Data from Old Server';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Start '.$this->description);
        Log::info('Starting data migration...');

        $data = DailyVerse::truncate();
        $data = DB::connection('old_mysql')->table('daily_verses')->get();
        foreach($data as $d) {
            $bible = new DailyVerse();
            $bible->book = $d->book;
            $bible->chapter = $d->chapter;
            $bible->verse = $d->verse;
            $bible->save();
        }

        Log::info($this->description.' finished');
        return Command::SUCCESS;
    }
}
