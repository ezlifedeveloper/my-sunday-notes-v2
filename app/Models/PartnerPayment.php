<?php

namespace App\Models;

use App\Traits\ModelCreator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PartnerPayment extends Model
{
    use HasFactory, ModelCreator;

    protected $fillable = [
        'user_id', 'type', 'qty', 'date',
        'value', 'desc', 'created_by'
    ];
}
