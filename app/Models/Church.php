<?php

namespace App\Models;

use App\Traits\ModelCreator;
use App\Traits\ModelPagination;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Church extends Model
{
    use HasFactory, ModelCreator, SoftDeletes, ModelPagination;

    protected $hidden = [
        'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_at'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value ? env('APP_URL').$value : asset('church-placeholder.png'),
        );
    }

    public function sinode()
    {
        return $this->belongsTo(Sinode::class, 'sinode_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->where('name', 'like', '%' . $search . '%')
            ->orWhere('address', 'like', '%' . $search . '%')
            ->orWhere('services', 'like', '%' . $search . '%');
    }
}
