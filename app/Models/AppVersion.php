<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppVersion extends Model
{
    use HasFactory;

    protected $fillable = [
        'version', 'platform', 'desc', 'date'
    ];

    public function scopeSearch($query, $search)
    {
        return $query->where('version', 'like', '%' . $search . '%')
            ->orWhere('platform', 'like', '%' . $search . '%')
            ->orWhere('desc', 'like', '%' . $search . '%')
            ->orWhere('date', 'like', '%' . $search . '%');
    }
}
