<?php

namespace App\Models;

use App\Traits\ModelCreator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sinode extends Model
{
    use HasFactory, ModelCreator, SoftDeletes;

    protected $hidden = [
        'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_at'
    ];

    protected $fillable = [
        'name', 'name_abbr', 'desc'
    ];

    public function churches()
    {
        return $this->hasMany(Church::class, 'sinode_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->where('name', 'like', '%' . $search . '%')
            ->orWhere('name_abbr', 'like', '%' . $search . '%')
            ->orWhere('desc', 'like', '%' . $search . '%');
    }
}
