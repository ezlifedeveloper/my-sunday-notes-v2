<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChurchRequest extends Model
{
    use HasFactory;

    public function scopeSearch($query, $search)
    {
        return $query->where('name', 'like', '%' . $search . '%');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function updator()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
