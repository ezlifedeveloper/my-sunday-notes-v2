<?php

namespace App\Traits;

trait UsingPagination
{
    public $perPage = 10;
}