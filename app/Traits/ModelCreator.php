<?php

namespace App\Traits;

use App\Models\User;

trait ModelCreator
{
    public function getDates()
    {
        return $this->dates + [
            'deleted_at'
        ];
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updator()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
