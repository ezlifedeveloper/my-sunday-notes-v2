<?php

namespace App\Traits;

trait ModelPagination
{
    public function scopePaginated($query, $limit, $page)
    {
        return $query->when($limit, function ($query) use ($limit) {
            return $query->take($limit);
        })->when($page, function ($query) use ($page, $limit) {
            return $query->skip(($page - 1) * $limit);
        });
    }
}