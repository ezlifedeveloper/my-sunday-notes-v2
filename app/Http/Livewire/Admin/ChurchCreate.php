<?php

namespace App\Http\Livewire\Admin;

use App\Helpers\Helper;
use App\Models\Church;
use App\Models\Sinode;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Livewire\WithFileUploads;

class ChurchCreate extends Component
{
    use WithFileUploads, LivewireAlert;

    public $dataId;
    public $inputName, $inputSinode, $inputPhone, $inputService;
    public $inputLocation, $inputLatitude, $inputLongitude, $inputAddress;
    public $inputWebsite, $inputPlayStore, $inputAppStore;
    public $inputInstagram, $inputTwitter, $inputFacebook;
    public $photo;

    public function mount($dataId = null)
    {
        $this->dataId = $dataId;
        if($dataId) {
            $data = Church::where('id', $dataId)->first();
            $this->inputName = $data->name;
            $this->inputSinode = $data->sinode_id;
            $this->inputPhone = $data->phone;
            $this->inputService = $data->services;
            $this->inputAddress = $data->address;
            $this->inputLatitude = $data->lat;
            $this->inputLongitude = $data->lng;
            $this->inputWebsite = $data->website;
            $this->inputPlayStore = $data->android;
            $this->inputAppStore = $data->ios;
            $this->inputInstagram = $data->instagram;
            $this->inputFacebook = $data->facebook;
            $this->inputTwitter = $data->twitter;
            $this->photo = $data->image;
        }
    }

    public function render()
    {
        return view('livewire.admin.church-create', [
            'title' => 'Church',
            'sinode' => Sinode::orderBy('name')->get()
        ])->layout('layouts.back', [
            'title' => 'Church'
        ]);
    }

    public function storeData()
    {
        if ($this->dataId){
            $data = Church::where('id', $this->dataId)->first();
        }
        else { 
            $data = new Church();
            $data->created_by = Auth::id();
        }

        $data->updated_by = Auth::id();
        $data->name = $this->inputName;
        $data->sinode_id = $this->inputSinode;
        $data->phone = $this->inputPhone;
        $data->services = $this->inputService;
        $data->address = is_array($this->inputAddress) ? $this->inputAddress[0] : $this->inputAddress;
        $data->lat = $this->inputLatitude;
        $data->lng = $this->inputLongitude;
        $data->website = $this->inputWebsite;
        $data->android = $this->inputPlayStore;
        $data->ios = $this->inputAppStore;
        $data->instagram = $this->inputInstagram;
        $data->facebook = $this->inputFacebook;
        $data->twitter = $this->inputTwitter;
        $data->slug = Helper::cleanstr($this->inputName);
        // dd($data);
        $data->save();

        if ($this->photo && !is_string($this->photo)) {
            $ext = '.' . $this->photo->getClientOriginalExtension();
            $filename = $data->slug . $ext;
            $this->photo->storeAs('public/church', $filename);
            Church::where('id', $data->id)->update(['image' => '/file/church/' . $filename]);
        }
    
        $this->alert('success', $this->dataId ? 'Data berhasil diperbarui' : 'Data telah direkam');
        return redirect()->route('back.church');
    }
}
