<?php

namespace App\Http\Livewire\Admin;

use App\Helpers\Helper;
use App\Models\Note as ModelsNote;
use App\Traits\UsingPagination;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Livewire\WithPagination;

class Note extends Component
{
    use WithPagination, LivewireAlert, UsingPagination;

    public $editData = 0;
    public $openForm = 0;

    public $search;

    public $selectedData;
    public $dataId;
    public $inputDate, $inputTitle, $inputTime, $inputLocation, 
        $inputEvent, $inputSpeaker, $inputVerse, $inputNote;

    public function render()
    {
        $data = ModelsNote::search($this->search)
            ->when(Helper::isAllowed(1), function ($query) {
                $query->withTrashed();
            })
            ->when(!Helper::isAllowed(1), function ($query) {
                $query->where('user_id', Auth::id());
            })
            ->orderBy('date', 'desc')
            ->paginate($this->perPage);

        if(Helper::isAllowed(1)) $header = ['No', 'Date', 'User', 'Title', 'Location', 'Event', 'Speaker', 'Verse', 'Last Update', ''];
        else $header = ['No', 'Date', 'Title', 'Location', 'Event', 'Speaker', 'Verse', 'Last Update', ''];

        return view('livewire.admin.note', [
            'data' => $data,    
            'total' => ModelsNote::withTrashed()->where('user_id', Auth::id())->count(),
            'title' => 'Note',
            'header' => $header
        ])->layout('layouts.back', [
            'title' => 'Note'
        ]);
    }

    private function resetValue()
    {
        $this->reset([
            'inputDate', 'inputTitle', 'inputTime', 'inputLocation', 
            'inputEvent', 'inputSpeaker', 'inputVerse', 'inputNote', 
            'dataId', 'selectedData'
        ]);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function deleteData($id)
    {
        $file = ModelsNote::withTrashed()->where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function restoreData($id)
    {
        $file = ModelsNote::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function viewDetail($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = ModelsNote::where('id', $id)->first();
            $this->selectedData = $data;
            $this->inputDate = $data->date;
            $this->inputTitle = $data->title;
            $this->inputTime = $data->time;
            $this->inputLocation = $data->location;
            $this->inputEvent = $data->event;
            $this->inputSpeaker = $data->speaker;
            $this->inputVerse = $data->verse;
            $this->inputNote = $data->note;
            $this->dataId = $data->id;
        } else if($this->editData && $id == null) {
            $this->openForm = true;
        } else{
            $this->resetValue();
        }
    }
}
