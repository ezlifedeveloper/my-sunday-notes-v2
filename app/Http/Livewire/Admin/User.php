<?php

namespace App\Http\Livewire\Admin;

use App\Models\User as ModelsUser;
use App\Models\UserRole;
use App\Traits\UsingPagination;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Livewire\WithPagination;

class User extends Component
{
    use WithPagination, LivewireAlert, UsingPagination;

    public $editData = 0;
    public $openForm = 0;

    public $search;

    public $dataId;
    public $inputName, $inputUsername, $inputEmail, $inputRole, $inputPassword;

    public function mount()
    {
        $this->perPage = 50;
    }

    public function render()
    {
        $data = ModelsUser::withTrashed()
            ->search($this->search)
            ->orderBy('id', 'desc')
            ->paginate($this->perPage);
        
        return view('livewire.admin.user', [
            'data' => $data,
            'total' => ModelsUser::count(),
            'role' => UserRole::all(),
            'title' => 'User'
        ])->layout('layouts.back', [
            'title' => 'User'
        ]);
    }

    private function resetValue()
    {
        $this->reset([
            'inputUsername', 'inputName', 'inputEmail', 'inputPassword', 
            'inputRole', 'dataId', 'search'
        ]);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function storeData()
    {
        if ($this->dataId){
            $data = ModelsUser::where('id', $this->dataId)->first();
        }
        else { 
            $data = new ModelsUser;
            $data->created_by = Auth::id();
        }

        $data->updated_by = Auth::id();
        $data->username = $this->inputUsername;
        $data->name = $this->inputName;
        $data->email = $this->inputEmail;
        $data->role_id = $this->inputRole;
        if($this->inputPassword) $data->password = bcrypt($this->inputPassword);
        $data->save();

        $this->resetValue();
        $this->alert('success', $this->dataId ? 'Data berhasil diperbarui' : 'Data telah direkam');
    }

    public function restoreData($id)
    {
        $file = ModelsUser::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = ModelsUser::withTrashed()->where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = ModelsUser::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputUsername = $data->username;
            $this->inputName = $data->name;
            $this->inputEmail = $data->email;
            $this->inputRole = $data->role_id;
        } else if($this->editData && $id == null) {
            $this->openForm = true;
        } else{
            $this->resetValue();
        }
    }
}
