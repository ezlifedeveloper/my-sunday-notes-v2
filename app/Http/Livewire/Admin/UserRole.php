<?php

namespace App\Http\Livewire\Admin;

use App\Models\UserRole as ModelsUserRole;
use App\Traits\UsingPagination;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Livewire\WithPagination;

class UserRole extends Component
{
    use WithPagination, LivewireAlert, UsingPagination;

    public $editData = 0;
    public $openForm = 0;

    public $search;

    public $dataId;
    public $inputName, $inputUsername, $inputEmail, $inputRole, $inputPassword;

    public function render()
    {
        $data = ModelsUserRole::withTrashed()
            ->search($this->search)
            ->paginate($this->perPage);
        
        return view('livewire.admin.user-role', [
            'data' => $data,
            'total' => ModelsUserRole::count(),
            'title' => 'User Role'
        ])->layout('layouts.back', [
            'title' => 'User Role'
        ]);
    }

    private function resetValue()
    {
        $this->reset([
            'inputName', 'dataId'
        ]);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function storeData()
    {
        if ($this->dataId){
            $data = ModelsUserRole::where('id', $this->dataId)->first();
        }
        else { 
            $data = new ModelsUserRole;
            $data->created_by = Auth::id();
        }

        $data->updated_by = Auth::id();
        $data->name = $this->inputName;
        $data->save();

        $this->resetValue();
        $this->alert('success', $this->dataId ? 'Data berhasil diperbarui' : 'Data telah direkam');
    }

    public function restoreData($id)
    {
        $file = ModelsUserRole::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = ModelsUserRole::withTrashed()->where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = ModelsUserRole::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputName = $data->name;
        } else if($this->editData && $id == null) {
            $this->openForm = true;
        } else{
            $this->resetValue();
        }
    }
}
