<?php

namespace App\Http\Livewire\Admin;

use App\Models\Sinode as ModelsSinode;
use App\Traits\UsingPagination;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Livewire\WithPagination;

class Sinode extends Component
{
    use WithPagination, LivewireAlert, UsingPagination;

    public $editData = 0;
    public $openForm = 0;

    public $search;

    public $dataId;
    public $inputName, $inputNameAbbr, $inputDesc;

    public function render()
    {
        $data = ModelsSinode::withTrashed()
            ->search($this->search)
            ->paginate($this->perPage);
        
        return view('livewire.admin.sinode', [
            'data' => $data,
            'total' => ModelsSinode::count(),
            'title' => 'Sinode'
        ])->layout('layouts.back', [
            'title' => 'Sinode'
        ]);
    }

    private function resetValue()
    {
        $this->reset([
            'inputName', 'inputNameAbbr', 'inputDesc', 'dataId'
        ]);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function storeData()
    {
        if ($this->dataId){
            $data = ModelsSinode::where('id', $this->dataId)->first();
        }
        else { 
            $data = new ModelsSinode;
            $data->created_by = Auth::id();
        }

        $data->updated_by = Auth::id();
        $data->name = $this->inputName;
        $data->name_abbr = $this->inputNameAbbr;
        $data->desc = $this->inputDesc;
        $data->save();

        $this->resetValue();
        $this->alert('success', $this->dataId ? 'Data berhasil diperbarui' : 'Data telah direkam');
    }

    public function restoreData($id)
    {
        $file = ModelsSinode::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = ModelsSinode::withTrashed()->where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = ModelsSinode::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputName = $data->name;
            $this->inputNameAbbr = $data->name_abbr;
            $this->inputDesc = $data->desc;
        } else if($this->editData && $id == null) {
            $this->openForm = true;
        } else{
            $this->resetValue();
        }
    }
}
