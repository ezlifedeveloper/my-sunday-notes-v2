<?php

namespace App\Http\Livewire\Admin;

use App\Models\AppReview as ModelsAppReview;
use App\Traits\UsingPagination;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Livewire\WithPagination;

class AppReview extends Component
{
    use WithPagination, LivewireAlert, UsingPagination;

    public $editData = 0;
    public $openForm = 0;

    public $search;

    public $dataId;
    public $inputName, $inputRating, $inputReview;

    public function render()
    {
        $data = ModelsAppReview::search($this->search)
            ->paginate($this->perPage);
        
        return view('livewire.admin.app-review', [
            'data' => $data,
            'total' => ModelsAppReview::count(),
            'title' => 'App Review',
            'rating' => ModelsAppReview::avg('rating')
        ])->layout('layouts.back', [
            'title' => 'App Review'
        ]);
    }

    private function resetValue()
    {
        $this->reset([
            'inputName', 'inputRating', 'inputReview', 'dataId'
        ]);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function storeData()
    {
        if ($this->dataId){
            $data = ModelsAppReview::where('id', $this->dataId)->first();
        }
        else { 
            $data = new ModelsAppReview;
        }

        $data->name = $this->inputName;
        $data->rating = $this->inputRating;
        $data->review = $this->inputReview;
        $data->save();

        $this->resetValue();
        $this->alert('success', $this->dataId ? 'Data berhasil diperbarui' : 'Data telah direkam');
    }

    public function restoreData($id)
    {
        $file = ModelsAppReview::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = ModelsAppReview::withTrashed()->where('id', $id)->delete();

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = ModelsAppReview::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputName = $data->name;
            $this->inputRating = $data->rating;
            $this->inputReview = $data->review;
        } else if($this->editData && $id == null) {
            $this->openForm = true;
        } else{
            $this->resetValue();
        }
    }
}
