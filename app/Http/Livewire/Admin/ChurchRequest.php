<?php

namespace App\Http\Livewire\Admin;

use App\Models\ChurchRequest as ModelsChurchRequest;
use App\Traits\UsingPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Livewire\WithPagination;

class ChurchRequest extends Component
{
    use WithPagination, LivewireAlert, UsingPagination;

    public $search;

    public function render()
    {
        $data = ModelsChurchRequest::search($this->search)
            ->latest()
            ->paginate($this->perPage);

        return view('livewire.admin.church-request', [
            'data' => $data,
            'total' => ModelsChurchRequest::count(),
            'title' => 'Church Request',
        ])->layout('layouts.back', [
            'title' => 'Church Request'
        ]);
    }
}
