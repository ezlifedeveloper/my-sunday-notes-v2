<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class NoteCreate extends Component
{
    public function render()
    {
        return view('livewire.admin.note-create');
    }
}
