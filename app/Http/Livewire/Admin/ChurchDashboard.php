<?php

namespace App\Http\Livewire\Admin;

use App\Models\Church;
use Livewire\Component;

class ChurchDashboard extends Component
{
    public function render()
    {
        $data = Church::all();
        return view('livewire.admin.church-dashboard', [
            'total' => count($data),
        ])->layout('layouts.back', [
            'title' => 'Church Dashboard'
        ]);
    }
}
