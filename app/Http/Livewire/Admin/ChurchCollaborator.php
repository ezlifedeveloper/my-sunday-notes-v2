<?php

namespace App\Http\Livewire\Admin;

use App\Models\PartnerPayment;
use App\Models\User;
use App\Traits\UsingPagination;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Livewire\WithPagination;

class ChurchCollaborator extends Component
{
    use WithPagination, LivewireAlert, UsingPagination;

    public $editData = 0;
    public $openForm = 0;

    public $search;
    public $dataId;

    public $inputQty, $inputValue, $inputDesc, $inputDate;

    public function render()
    {
        return view('livewire.admin.church-collaborator', [
            'data' => User::with(['churchInput'])->where('role_id', 3)
                ->paginate($this->perPage),
            'total' => User::where('role_id', 3)->count(),
            'title' => 'Collaborator'
        ])->layout('layouts.back', [
            'title' => 'Church Collaborator'
        ]);
    }

    private function resetValue()
    {
        $this->reset([
            'inputQty', 'inputValue', 'inputDesc', 
            'inputDate', 'dataId'
        ]);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function submitPayment()
    {
        $data = new PartnerPayment;
        $data->created_by = Auth::id();
        $data->user_id = $this->dataId;
        $data->type = 'church';
        $data->date = $this->inputDate;
        $data->qty = $this->inputQty;
        $data->value = $this->inputValue;
        $data->desc = $this->inputDesc;
        $data->save();

        $this->resetValue();
        $this->alert('success', 'Data telah direkam');
    }

    public function togglePayData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
        } else{
            $this->resetValue();
        }
    }
}
