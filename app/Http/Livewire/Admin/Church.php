<?php

namespace App\Http\Livewire\Admin;

use App\Helpers\Helper;
use App\Models\Church as ModelsChurch;
use App\Models\Sinode;
use App\Traits\UsingPagination;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Livewire\WithPagination;

class Church extends Component
{
    use WithPagination, LivewireAlert, UsingPagination;

    public $editData = 0;
    public $openForm = 0;

    public $search;

    public $dataId;
    public $inputName, $inputSinode, $inputPhone, $inputService;

    public function render()
    {
        $data = ModelsChurch::withTrashed()
            ->search($this->search)
            ->latest()
            ->paginate($this->perPage);
        
        return view('livewire.admin.church', [
            'data' => $data,
            'total' => ModelsChurch::count(),
            'title' => 'Church',
            'sinode' => Sinode::orderBy('name')->get()
        ])->layout('layouts.back', [
            'title' => 'Church'
        ]);
    }

    private function resetValue()
    {
        $this->reset([
            'inputName', 'inputSinode', 'inputPhone', 'dataId',
            'inputService'
        ]);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function restoreData($id)
    {
        $file = ModelsChurch::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = ModelsChurch::withTrashed()->where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function setVerified($id, $state)
    {
        $data = ModelsChurch::where('id', $id)
            ->update(['is_verified' => $state]);
        $this->alert('success', 'Data berhasil diperbarui');
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = ModelsChurch::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputName = $data->name;
            $this->inputSinode = $data->sinode_id;
            $this->inputPhone = $data->phone;
            $this->inputService = $data->service;
        } else if($this->editData && $id == null) {
            $this->openForm = true;
        } else{
            $this->resetValue();
        }
    }
}
