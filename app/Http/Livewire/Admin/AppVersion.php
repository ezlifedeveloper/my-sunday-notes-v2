<?php

namespace App\Http\Livewire\Admin;

use App\Models\AppVersion as ModelsAppVersion;
use App\Traits\UsingPagination;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Livewire\WithPagination;

class AppVersion extends Component
{
    use WithPagination, LivewireAlert, UsingPagination;

    public $editData = 0;
    public $openForm = 0;

    public $search;

    public $dataId;
    public $inputPlatform, $inputDesc, $inputVersion, $inputDate;

    public function render()
    {
        $data = ModelsAppVersion::search($this->search)
            ->paginate($this->perPage);
        $dataTotal = ModelsAppVersion::all();
        
        return view('livewire.admin.app-version', [
            'data' => $data,
            'total' => $dataTotal->count(),
            'totalMobile' => $dataTotal->where('platform', 1)->count(),
            'totalWeb' => $dataTotal->where('platform', 0)->count(),
            'title' => 'App Version',
        ])->layout('layouts.back', [
            'title' => 'App Version'
        ]);
    }

    private function resetValue()
    {
        $this->reset([
            'inputPlatform', 'inputDesc', 'inputVersion', 'inputDate', 'dataId'
        ]);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function storeData()
    {
        if ($this->dataId){
            $data = ModelsAppVersion::where('id', $this->dataId)->first();
        }
        else { 
            $data = new ModelsAppVersion;
            $data->created_by = Auth::id();
        }

        $data->updated_by = Auth::id();
        $data->platform = $this->inputPlatform;
        $data->desc = $this->inputDesc;
        $data->version = $this->inputVersion;
        $data->date = $this->inputDate;
        $data->save();

        $this->resetValue();
        $this->alert('success', $this->dataId ? 'Data berhasil diperbarui' : 'Data telah direkam');
    }

    public function restoreData($id)
    {
        $file = ModelsAppVersion::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = ModelsAppVersion::withTrashed()->where('id', $id)->delete();

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = ModelsAppVersion::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputPlatform = $data->platform;
            $this->inputDesc = $data->desc;
            $this->inputVersion = $data->version;
            $this->inputDate = $data->date;
        } else if($this->editData && $id == null) {
            $this->openForm = true;
        } else{
            $this->resetValue();
        }
    }
}
