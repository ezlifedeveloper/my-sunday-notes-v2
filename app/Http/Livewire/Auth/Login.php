<?php

namespace App\Http\Livewire\Auth;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    /** @var string */
    public $username = '';

    /** @var string */
    public $password = '';

    /** @var bool */
    public $remember = false;

    protected $rules = [
        'username' => ['required'],
        'password' => ['required'],
    ];

    public function mount()
    {
        if (Auth::check()) return Helper::isAllowed(1) ? redirect()->intended(route('back.dashboard')) : redirect()->intended(route('back.profile'));
    }

    public function render()
    {
        return view('livewire.auth.login')
            ->layout('layouts.base');
    }

    public function authenticate()
    {
        $this->validate();
        if (!Auth::attempt(['username' => $this->username, 'password' => $this->password], $this->remember)) {
            $this->addError('username', trans('auth.failed'));

            Helper::recordUserLogin(null, $this->username, 'failed', 'website');
            return;
        }

        Helper::recordUserLogin(Auth::id(), $this->username, 'ok', 'website');
        return Helper::isAllowed(1) ? redirect()->intended(route('back.dashboard')) : redirect()->intended(route('back.profile'));
    }
}
