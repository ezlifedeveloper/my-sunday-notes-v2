<?php

namespace App\Http\Livewire\Etc;

use Illuminate\Support\Facades\Storage;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Livewire\WithFileUploads;

class GenerateAdk extends Component
{
    use WithFileUploads, LivewireAlert;
    
    public $file;
    public $successLoad = false;
    public $timestamp;
    public $xmlPath, $csvPath;

    public function render()
    {
        return view('livewire.etc.generate-adk')
            ->layout('layouts.base');
    }

    public function load()
    {
        $this->validate([
            'file' => 'required|file|mimes:json'
        ]);

        if ($this->file && !is_string($this->file)) {
            // store file
            $timestamp = now()->timestamp;
            Storage::put('etc/adk-'.$timestamp.'.json', $this->file->get());

            // load json
            $jsonContent = Storage::get('etc/adk-'.$timestamp.'.json');
            $jsonData = json_decode($jsonContent, true);
            $data = json_decode(json_encode($this->convertObjectKeysToLowercase($jsonData[array_key_first($jsonData)])), true);

            // dd($data[0]);
            $filename = 'd_item'.$data[0]['kddept'].$data[0]['kdunit'].$data[0]['kdib'].$data[0]['kdsatker'].'0';
            $this->timestamp = $filename;
            // Convert array to XML
            $xmlContent = $this->arrayToPrettyXml($data);

            // Define the file path
            $xmlFilePath = 'etc/'.$filename.'.xml';
            $this->xmlPath = '/etcfile/'.$filename.'.xml';

            // Save XML content to file
            Storage::put($xmlFilePath, $xmlContent);

            // Convert array to CSV
            // Define the file path
            $csvFilePath = 'etc/'.$filename.'.csv';
            $this->csvPath = '/etcfile/'.$filename.'.csv';

            // Open a file in write mode
            $file = fopen(storage_path('app/'.$csvFilePath), 'w');

            // Write each row to the CSV
            foreach ($data as $row) {
                $row = $this->addCharToArray($row, '^'); // Add double quotes to each value
                fputcsv($file, $row, '|');
            }

            // Close the file
            fclose($file);

            $this->successLoad = true;
            $this->alert('success', 'File loaded successfully');
        } else {
            $this->alert('danger', 'Failed to load file');
            return redirect()->route('etc.generate-adk');
        }
    }

    private function arrayToPrettyXml(array $data, string $rootElement = 'VFPData'): string
    {
        $dom = new \DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = true; // Enable pretty printing
        $dom->xmlStandalone = true; // Set standalone="yes"

        // Add root element
        $root = $dom->createElement($rootElement);
        $dom->appendChild($root);
        $root->setAttribute('xml:space', 'preserve');

        // $staticPath = storage_path('app/etc/default-item.xml');
        // $xmlContent = file_get_contents($staticPath);
        // $staticXml = simplexml_load_string($xmlContent);
        
        $staticFragment = $dom->createDocumentFragment();
        $staticFragment->appendXML(file_get_contents(storage_path('app/etc/default-item.xml')));
        $root->appendChild($staticFragment);

        // Recursively build XML
        $this->buildXml($dom, $root, $data);

        return $dom->saveXML();
    }

    private function buildXml(\DOMDocument $dom, \DOMElement $parent, array $data)
    {
        foreach ($data as $key => $value) {
            // Handle numeric keys
            if (is_numeric($key)) {
                $key = 'c_item'; // Replace numeric keys with a valid tag name
            }

            // Create element
            $element = $dom->createElement($key);

            if (is_array($value)) {
                // Recursively handle nested arrays
                $this->buildXml($dom, $element, $value);
            } else {
                // Add text content to the element
                $element->appendChild($dom->createTextNode($value));
            }

            // Append the element to the parent
            $parent->appendChild($element);
        }
    }

    private function convertObjectKeysToLowercase($data)
    {
        if (is_object($data)) {
            $data = (array) $data; // Convert object to array for processing
        }

        if (is_array($data)) {
            $lowercaseData = [];
            foreach ($data as $key => $value) {
                // Convert key to lowercase
                $lowerKey = strtolower($key);

                // Recursively handle nested objects/arrays
                $lowercaseData[$lowerKey] = is_object($value) || is_array($value)
                    ? $this->convertObjectKeysToLowercase($value)
                    : $value;
            }

            return (object) $lowercaseData; // Convert back to object
        }

        // Return data if it's not an array or object
        return $data;
    }

    private function addCharToArray(array $array, string $char): array
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $array[$key] = $this->addCharToArray($value, $char);
            } else {
                $array[$key] = $char . $value .$char;
            }
        }

        return $array;
    }
}
