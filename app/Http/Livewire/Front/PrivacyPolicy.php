<?php

namespace App\Http\Livewire\Front;

use Livewire\Component;

class PrivacyPolicy extends Component
{
    public function render()
    {
        $description = 'Our privacy policy';
        return view('livewire.front.privacy-policy')
            ->layout('layouts.app',  [
                'title' => 'Privacy Policy',
                'description' => $description,
                'type' => 'Privacy Policy'
            ]);
    }
}
