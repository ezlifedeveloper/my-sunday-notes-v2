<?php

namespace App\Http\Livewire\Front;

use Livewire\Component;

class Post extends Component
{
    public function render()
    {
        $description = 'Our post to inform you about the great and latest news or article.';
        return view('livewire.front.post')
            ->layout('layouts.app',  [
                'title' => 'Post',
                'description' => $description,
                'type' => 'Productivity'
            ]);
    }
}
