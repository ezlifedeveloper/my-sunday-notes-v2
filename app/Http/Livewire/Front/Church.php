<?php

namespace App\Http\Livewire\Front;

use App\Models\Church as ModelsChurch;
use App\Models\Sinode;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Church extends Component
{
    public $search;
    protected $queryString = ['search'];

    public function render()
    {
        $description = 'Our database keeps growing to help you find the nearby churches!';
        $data = ModelsChurch::selectRaw('id, name, services, image, viewed, slug')
            ->where('name', 'like', '%'.$this->search.'%')
            ->orderBy('viewed', 'desc')
            ->limit(12)
            ->get();
        $mostViewed = ModelsChurch::orderBy('viewed', 'desc')
            ->first();
        return view('livewire.front.church',[
            'data' => $data,
            'total' => ModelsChurch::count(),
            'sinode' => Sinode::count(),
            'viewed' => $mostViewed->name
        ])->layout('layouts.app', [
            'title' => 'Church',
            'description' => $description,
            'type' => 'Productivity'
        ]);
    }
}
