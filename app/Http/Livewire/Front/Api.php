<?php

namespace App\Http\Livewire\Front;

use Livewire\Component;

class Api extends Component
{
    public function render()
    {
        $description = 'API for developer to collaborate in enchancing community and data';
        return view('livewire.front.api')
            ->layout('layouts.app',  [
                'title' => 'API',
                'description' => $description,
                'type' => 'API'
            ]);
    }
}
