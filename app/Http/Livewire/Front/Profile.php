<?php

namespace App\Http\Livewire\Front;

use Livewire\Component;

class Profile extends Component
{
    public function render()
    {
        $description = 'We are My Sunday Notes';
        return view('livewire.front.profile')
            ->layout('layouts.app',  [
                'title' => 'Profile',
                'description' => $description,
                'type' => 'Company Profile'
            ]);
    }
}
