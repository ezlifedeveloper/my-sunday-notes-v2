<?php

namespace App\Http\Livewire\Front;

use App\Models\Church;
use Livewire\Component;

class ChurchDetail extends Component
{
    public $slug;
    
    public function mount($slug)
    {
        $this->slug = $slug;
        $updateViewCount = Church::where('slug', $slug)->toBase()->increment('viewed');
    }

    public function render()
    {
        $data = Church::where('slug', $this->slug)->first();
        return view('livewire.front.church-detail', [
            'data' => $data
        ])->layout('layouts.app', [
            'description' => $data->name.' | '.$data->address,
            'title' => $data->name,
            'type' => 'Church',
            'imgasset' => isset($data->image) ? env('APP_URL').$data->image : null
        ]);
    }
}
