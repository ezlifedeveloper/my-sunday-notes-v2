<?php

namespace App\Http\Livewire\Front;

use App\Models\AppReview as ModelsAppReview;
use Livewire\Component;

class AppReview extends Component
{
    public function render()
    {
        return view('livewire.front.app-review', [
            'data' => ModelsAppReview::whereNotNull('review')->inRandomOrder()->first()
        ]);
    }
}
