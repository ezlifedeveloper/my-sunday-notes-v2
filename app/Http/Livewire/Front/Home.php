<?php

namespace App\Http\Livewire\Front;

use App\Models\Church;
use App\Models\Note;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Home extends Component
{
    public $search;

    public function render()
    {
        $description = 'My Sunday Notes will help you to find churches and make your own notes';
        return view('livewire.front.home',[
            'user_count' => User::count(),
            'church_count' => Church::count(),
            'note_count' => Note::count()
        ])->layout('layouts.app', [
            'description' => $description,
            'type' => 'Productivity'
        ]);
    }

    public function search()
    {
        return redirect()->to('/church?search='.$this->search);
    }
}
