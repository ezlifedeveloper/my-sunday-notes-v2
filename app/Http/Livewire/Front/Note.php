<?php

namespace App\Http\Livewire\Front;

use Livewire\Component;

class Note extends Component
{
    public function render()
    {
        $description = 'Our note is well-templated to increase your experience in creating it.';
        return view('livewire.front.note')
            ->layout('layouts.app',  [
                'title' => 'Note',
                'description' => $description,
                'type' => 'Productivity'
            ]);
    }
}
