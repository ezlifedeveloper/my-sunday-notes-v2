<?php

namespace App\Http\Livewire\Front;

use Livewire\Component;

class Career extends Component
{
    public function render()
    {
        $description = 'Fill your free time productively with us and help the community!.';
        return view('livewire.front.career')
            ->layout('layouts.app',  [
                'title' => 'Career',
                'description' => $description,
                'type' => 'Career'
            ]);
    }
}
