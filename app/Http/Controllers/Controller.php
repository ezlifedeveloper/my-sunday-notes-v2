<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function jsonResponsePaginated($success, $code, $message, $data, $totalData, $limit, $page)
    {
        return response()
            ->json([
                'success' => $success, 
                'result' => $message,
                'total' => (int) $totalData,
                'perPage' => (int) ($limit ?? $totalData),
                'currentPage' => (int) ($page ?? 1),
                'lastPage' => ceil($totalData / (($limit ?? $totalData) <= 0 ? 1 : ($limit ?? $totalData))),    
                'data' => $data
            ], $code);
    }

    public function jsonResponse($success, $code, $message, $data = null)
    {
        if($data) return response()->json(['success' => $success, 'result' => $message, 'data' => $data], $code);
        else return response()->json(['success' => $success, 'result' => $message], $code);
    }

    public function jsonResponseMissingParameter()
    {
        return response()->json(['success' => false, 'result' => 'Missing Parameter'], 500);
    }
}
