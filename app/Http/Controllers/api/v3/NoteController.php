<?php

namespace App\Http\Controllers\api\v3;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NoteController extends Controller
{
    /**
     * Get Note Data.
     *
     * Return the Note data
     */
    public function data(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // Keyword search. Example: GPI
            'search' => 'string',
            // Note ID. Example: 1
            'id' => 'string',
            // The User's ID. Example: 2
            'user_id' => 'required|string',
            // Data Per Page
            'limit' => 'required|int',
            // Page Number
            'page' => 'required|int',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $search = $request->search ?? null;
        $limit = $request->limit ?? null;
        $page = $request->page ?? ($request->limit ? 1 : null);

        $id = $request->id;
        $user_id = $request->user_id;

        $totalData = Note::where('user_id', '=' ,$user_id)
        ->when($id, function ($query) use ($id) {
            $query->where('id', $id);
        })->when($search, function ($query) use ($search) {
            $query->where( function ( $query ) use ($search) {
                $query->where('title', 'like', '%'.$search.'%');
                $query->orWhere('location', 'like', '%'.$search.'%');
                $query->orWhere('speaker', 'like', '%'.$search.'%');
                $query->orWhere('verse', 'like', '%'.$search.'%');
            });
        })->count();

        $note = Note::where('user_id', '=' ,$user_id)
        ->when($id, function ($query) use ($id) {
            $query->where('id', $id);
        })->when($search, function ($query) use ($search) {
            $query->where( function ( $query ) use ($search) {
                $query->where('title', 'like', '%'.$search.'%');
                $query->orWhere('location', 'like', '%'.$search.'%');
                $query->orWhere('speaker', 'like', '%'.$search.'%');
                $query->orWhere('verse', 'like', '%'.$search.'%');
            });
        })->when(!$id && $limit, function ($query) use ($limit) {
            return $query->take($limit);
        })->when(!$id && $page, function ($query) use ($page, $limit) {
            return $query->skip(($page - 1) * $limit);
        })->orderBy('date', 'desc')->orderBy('created_at', 'desc')->get();

        Helper::recordApiLog($user_id, 'V3 note/data', 'OK');
        return $this->jsonResponsePaginated(true, 200, 'Succesfully Get Note Data', $note, $totalData, $limit, $page);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // Note ID. Example: 1
            'id' => 'required|string',
            // The User's ID. Example: 2
            'user_id' => 'required|string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $note = Note::where('id', $request->id)
            ->update([
                'date' => $request->date,
                'user_id' => $request->user_id,
                'time' => $request->time,
                'location' => $request->location,
                'event' => $request->event,
                'speaker' => $request->speaker,
                'verse' => $request->verse,
                'note' => $request->note,
                'title' => $request->title,
                'created_by' => $request->user_id,
                'updated_by' => $request->user_id,
            ]);

        $note = Note::where('id', $request->id)->first();

        Helper::recordApiLog($request->user_id, 'V3 note/save', 'OK');
        return $this->jsonResponse(true, 200, 'Succesfully Save Note', $note);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // Date. Example: 2023-02-19
            'date' => 'required|string',
            // The User's ID. Example: 2
            'user_id' => 'required|string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $note = new Note;
        $note->date = $request->date;
        $note->user_id = $request->user_id;
        $note->time = $request->time;
        $note->location = $request->location;
        $note->event = $request->event;
        $note->speaker = $request->speaker;
        $note->verse = $request->verse;
        $note->note = $request->note;
        $note->title = $request->title;
        $note->created_by = $request->user_id;
        $note->updated_by = $request->user_id;
        $note->save();

        Helper::recordApiLog($request->user_id, 'V3 note/create', 'OK');
        return $this->jsonResponse(true, 200, 'Succesfully Create Note', $note);
    }
}