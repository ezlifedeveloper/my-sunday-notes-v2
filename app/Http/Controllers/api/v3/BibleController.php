<?php

namespace App\Http\Controllers\api\v3;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Bible;
use App\Models\BibleList;
use App\Models\DailyVerse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**
 * @group Bible
 *
 * APIs for managing Bible data
 */
class BibleController extends Controller
{
    /**
     * Get Bible Data.
     *
     * Return the bible data by version
     */
    public function getData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // The version of the bible. Example: TB
            'version' => 'required|string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $version = BibleList::where('name', $request->version)->first();
        if(!$version) return $this->jsonResponse(false, 500, 'Version not found'); 

        $bible = Bible::with(['version'])->where('version_id', $version->id)->get();

        Helper::recordApiLog(null, 'V3 bible/data', 'OK');
        return $this->jsonResponse(true, 200, 'Succesfully Get Bible Data', $bible);
    }

    /**
     * Check Bible Version Data.
     *
     * Return the bible version data and the status whether it is need to be synchronized or not
     */
    public function checkVersion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // Bible version count. Example: 2
            'num' => 'required|string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $bible = BibleList::get()->count();
        $result = $request->num!=$bible ? 'sync' : 'no';
        $bible = BibleList::get();

        Helper::recordApiLog(null, 'V3 bible/version', 'OK');
        return $this->jsonResponse(true, 200, $result, $bible);
    }

    /**
     * Get Daily Verse Data.
     *
     * Return the daily verse data for a year
     */
    public function getYearlyVerseData(Request $request)
    {
        $bible = DailyVerse::orderByRaw('rand()')->limit(366)->get();

        Helper::recordApiLog(null, 'V3 bible/dailyverse', 'OK');
        return $this->jsonResponse(true, 200, 'Succesfully Get Daily Verse Data', $bible);
    }
}