<?php

namespace App\Http\Controllers\api\v3;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Church;
use App\Models\ChurchRequest;
use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChurchController extends Controller
{
    /**
     * Get Church Data.
     *
     * Return the Church data
     */
    public function getData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // Keyword search. Example: GPI
            'search' => 'string',
            // The User's latitude
            'lat' => 'required|string',
            // The User's longitude
            'lng' => 'required|string',
            // Data Per Page
            'limit' => 'required|int',
            // Page Number
            'page' => 'required|int',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $search = $request->search ?? null;
        $lat = $request->lat;
        $lng = $request->lng;
        $limit = $request->limit ?? null;
        $page = $request->page ?? ($request->limit ? 1 : null);

        $totalData = Church::when($search, function ($query) use ($search) {
            $query->search($search);
        })->count();

        $church = Church::selectRaw('*, (ACOS(COS(RADIANS(' . $lat . '))* COS(RADIANS(lat))* COS(RADIANS(lng)-RADIANS(' . $lng . '))+SIN(RADIANS(' . $lat . '))*SIN(RADIANS(lat)))*6371) AS distance')
            ->when($search, function ($query) use ($search) {
                $query->search($search);
            })->paginated($limit, $page)
            ->orderBy('distance', 'asc')->get();

        Helper::recordApiLog(null, 'V3 church/data', 'OK');
        return $this->jsonResponsePaginated(true, 200, 'Succesfully Get Church Data', $church, $totalData, $limit, $page);
    }

    /**
     * Send Church Request.
     *
     * Return the Church data
     */
    public function request(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // The User's ID
            'user_id' => 'required|string',
            // Church's name. Example: GPIB Bethesda Sidoarjo
            'name' => 'required|string',
            // The User's latitude
            'lat' => 'required|string',
            // The User's longitude
            'lng' => 'required|string',
            // Request status. Example: update / new
            'status' => 'required|string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $church = new ChurchRequest();
        $church->name = $request->name;
        $church->phone = $request->phone;
        $church->address = $request->address;
        $church->services = $request->service;
        $church->lat = $request->lat;
        $church->lng = $request->lng;
        $church->type = $request->status;
        $church->user_id = $request->user_id;
        $church->church_id = $request->church_id;
        $church->tracking = 'issued';

        if ($request->has('file')) {
            $ext = '.' . $request->file->getClientOriginalExtension();
            $filename = Helper::cleanStr($request->name).'-requested-'.time().$ext;
            $request->file->storeAs('public/church', $filename);
            $church->image = '/file/church/' . $filename;
        }

        $church->save();

        Helper::recordApiLog($request->id_user, 'V3 church/request', 'OK');
        return $this->jsonResponse(true, 200, 'Succesfully Send Church Request', $church);
    }
}
