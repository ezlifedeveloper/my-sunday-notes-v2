<?php

namespace App\Http\Controllers\api\v3;

use App\Helpers\Helper;
use http\Env\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Http\Requests\DataListRequest;
use App\Http\Requests\EmailRequest;
use Illuminate\Http\Request;
use App\Mail\ResetPassword;
use App\Mail\VerifyEmail;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Get User Data.
     *
     * Return the User data
     */
    public function getData(DataListRequest $request)
    {
        $request->validated();
        
        $search = $request->search ?? null;
        $limit = $request->limit ?? null;
        $page = $request->page ?? ($request->limit ? 1 : null);

        $totalData = User::when($search, function ($query) use ($search) {
            $query->where('name', 'like', '%' . $search . '%');
            $query->orWhere('username', 'like', '%' . $search . '%');
            $query->orWhere('email', 'like', '%' . $search . '%');
        })->count();

        $user = User::when($search, function ($query) use ($search) {
            $query->search($search);
        })->paginated($limit, $page)
        ->orderBy('name', 'asc')->get();

        Helper::recordApiLog(null, 'V3 user/data', 'OK');
        return $this->jsonResponsePaginated(true, 200, 'Succesfully Get User Data', $user, $totalData, $limit, $page);
    }

    /**
     * Register User.
     *
     * Return the User data
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // User's name
            'name' => 'required|string',
            // User's Username
            'username' => 'required|string',
            // User's Birth Date
            'birth_date' => 'required|string',
            // User's Sex (M/L)
            'sex' => 'required|string',
            // User's Email
            'email' => 'required|string',
            // User's Password
            'password' => 'required|string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $cekusername = User::where('username', $request->username)->first();
        if($cekusername){
            Helper::recordApiLog(null, 'V3 /register', 'failed');
            return $this->jsonResponse(false, 500, 'Username already registered!');
        }
        
        $cekemail = User::where('email', $request->email)->first();
        if($cekemail) {
            Helper::recordApiLog(null, 'V3 user/register', 'failed');
            return $this->jsonResponse(false, 500, 'Email already registered!');
        }

        $user = new User;
        $user->name = $request->name;
        $user->username = $request->username;
        $user->birth_date = $request->birth_date;
        $user->sex = $request->sex;
        $user->email = $request->email;
        $user->role_id = 2;
        $user->password = bcrypt($request->password);
        $user->save();

        $hashed = bcrypt($request->email.$request->username);
        $url = env('APP_URL').'/verify?p='.$user->id.'&q='.$hashed;
        Mail::to($request->email)->send(new VerifyEmail($url));

        Helper::recordApiLog(null, 'V3 user/register', 'OK');
        return $this->jsonResponse(true, 200, 'User Registered. Please check your email to verify your account', $user);
    }

    /**
     * Update User Data.
     *
     * Return the User data
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // User's ID
            'id' => 'required|string',
            // User's name
            'name' => 'required|string',
            // User's Username
            'username' => 'required|string',
            // User's Birth Date
            'birth_date' => 'required|string',
            // User's Sex (M/L)
            'sex' => 'required|string',
            // User's Email
            'email' => 'required|string',
            // User's Password
            'password' => 'string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $user = User::where('id', $request->id)
            ->update([
                'name' => $request->name,
                'username' => $request->username,
                'birth_date' => $request->birth_date,
                'sex' => $request->sex,
                'email' => $request->email,
            ]);

        if(isset($request->password)){
            $user = User::where('id', $request->id)
                ->update(['password' => bcrypt($request->password)]);
        }

        $user = User::where('id', $request->id)->first();
        Helper::recordApiLog($request->id, 'V3 /update', 'OK');
        return $this->jsonResponse(true, 200, 'User Profile Saved', $user);
    }

    private function generateToken($username, $email)
    {  
        $token = hash('sha256', $username.$email);
        $user = User::where('username', $username)->update(['api_token' => $token]);
        return $token;
    }

    /**
     * Login.
     *
     * Return the User data
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // User's Username
            'username' => 'required|string',
            // User's Password
            'password' => 'required|string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $username = $request->username;
        $pwd = $request->password;
        
        $auth = User::where('username',$username)->first();
                        
        if($auth && Hash::check($pwd, $auth->password) && $auth->email_verified_at){
            $auth['api_token'] = $this->generateToken($auth->username, $auth->email);
            Helper::recordApiLog($auth->id, 'V3 login', 'OK');

            Helper::recordUserLogin($auth->id, $auth->username, 'ok', 'mobile');
            return $this->jsonResponse(true, 200, 'Login Success', $auth);
        }
        else if(!$auth){
            Helper::recordApiLog(null, 'V3 login', 'OK');
            Helper::recordUserLogin(null, $username, 'ID not registered', 'mobile');
            return $this->jsonResponse(false, 500, 'ID not registered');
        }
        else if(!$auth->email_verified_at){
            Helper::recordApiLog($auth->id, 'V3 login', 'OK');
            Helper::recordUserLogin($auth->id, $auth->username, 'Account not verified', 'mobile');
            return $this->jsonResponse(false, 500, 'Account not verified, please activate your account');
        }
        else if(!Hash::check($pwd, $auth->password)){
            Helper::recordApiLog(null, 'V3 login', 'OK');
            Helper::recordUserLogin(null, $auth->username, 'Password and id combination not match', 'mobile');
            return $this->jsonResponse(false, 500, 'Password and id combination not match');
        }
    }

    /**
     * Login using Google SSO.
     *
     * Return the User data
     */
    public function loginGoogle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // User's Google Email
            'email' => 'required|string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $cekemail = User::where('email', $request->email)->first();

	    if(!$cekemail)
        {
            $username = substr($request->email, 0, strpos($request->email, '@'));
            $cekusername = User::where('username', $username)->first();

            $tokztokz = $this->generateToken($username, $request->email);

            if($cekusername){
                $lastid = User::select('id')->orderBy('id', 'desc')->first();
                $username = $username.$lastid->id;
            } 

            $result = 'User Registered';
            $user = new User;
            $user->name = $username;
            $user->username = $username;
            $user->email = $request->email;
            $user->role_id = 2;
            // $user->password = bcrypt($request->password);
            $user->api_token = hash('sha256', $tokztokz);
            $user->email_verified_at = date("Y-m-d h:i:s", time());
            $user->save();

            $user->token = $tokztokz;
        }
	
        $result = 'Login Success';
        $data = User::where('email', $request->email)->first();
        // dd($data);
        
        $data->token = $this->generateToken($data->username, $data->email);
        Helper::recordApiLog($data->id, 'V3 google', 'OK');
        
        return $this->jsonResponse(true, 200, $result, $data);
    }

    /**
     * Login using Google SSO.
     *
     * Return the User data
     */
    public function logout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // User's Username
            'username' => 'required|string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $username = $request->username;
        
        $auth = User::where('username',$username)->update(['api_token'=>NULL]);
        $auth = User::where('username',$username)->first();
        Helper::recordApiLog($auth->id, 'V3 logout', 'OK');
                        
        return response()->json(['status'=> 200, 'result'=>'Logout Success']);
    }

    /**
     * Reset Password
     *
     * Return the status of email verification reset
     */
    public function sendReset(EmailRequest $request)
    {
        $request->validated();
        $user = User::where('email', $request->email)->first();

        if($user) {
            $hashed = bcrypt($user->email.$user->username);
            $url = env('APP_URL').'/reset?p='.$user->id.'&q='.$hashed;
            Mail::to($request->email)->send(new ResetPassword($url));

            Helper::recordApiLog($user->id, 'V3 sendreset', 'OK');
            return response()->json(['status'=> 200, 'result'=>'Reset password request has been sent to your email']);
        }
        else {
            Helper::recordApiLog($user->id, 'V3 sendreset', 'Email not registered');
            return response()->json(['status'=> 500, 'result'=>'Email not registered']);
        }
    }
}