<?php

namespace App\Http\Controllers\api\v3;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Church;
use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    /**
     * Get Dashboard Data.
     *
     * Return the User's Dashboard data
     */
    public function getMainData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // The User's ID. Example: 2
            'user_id' => 'required|string',
            // The User's latitude
            'lat' => 'required|string',
            // The User's longitude
            'lng' => 'required|string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $iduser = $request->user_id;
        $lat = $request->lat;
        $lng = $request->lng;
        $earths_radius = 6371;
        $distance = 100;

        $nearby = Church::with(['sinode'])
            ->selectRaw('*, (ACOS(COS(RADIANS('.$lat.'))* COS(RADIANS(lat))* COS(RADIANS(lng)-RADIANS('.$lng.'))+SIN(RADIANS('.$lat.'))*SIN(RADIANS(lat)))*6371) AS distance')
            ->orderBy('distance', 'asc')->limit(1)->get();
                
        $data['nearby'] = $nearby[0];
        
        $data['countdif'] = Note::where('user_id', $iduser)->distinct('location')->count('location');
        $countmax = Note::select('location', DB::raw('count(*) as total'))->where('user_id', $iduser)->orderBy('total', 'desc')->groupBy('location')->first();
        if($countmax) $data['countmax'] = $countmax->location.'|'.$countmax->total;
        else $data['countmax'] = '-|0';

        Helper::recordApiLog($request->id_user, 'V3 maindata', 'OK');
        return $this->jsonResponse(true, 200, 'Succesfully Get Main Data', $data);
    }

    public function checkChurchUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // Latest Date in DB
            'date' => 'required|date',
            // Church Data Count
            'num' => 'required|string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();

        $church = Church::select('updated_at')->orderBy('updated_at', 'desc')->first();
        $count = Church::select('id')->get()->count();
        $datechurch = substr($church->updated_at, 0, 10);

        if($church){
            if($datechurch > $request->date || $count != $request->num) $result = 'sync down';
            else $result = 'no';
        } else $result = 'sync down';

        Helper::recordApiLog(null, 'V3 church/check', 'OK');
        return $this->jsonResponse(true, 200, 'Succesfully Check Church Update', $result);
    }

    public function checkNoteUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // The User's ID. Example: 2
            'user_id' => 'required|string',
            // Latest Date in DB
            'date' => 'required|date',
            // Church Data Count
            'num' => 'required|string',
        ]);
        if($validator->fails()) return $this->jsonResponseMissingParameter();
        
        $note = Note::select('updated_at')->where('user_id', $request->user_id)->orderBy('updated_at', 'desc')->first();
        $count = Note::select('id')->where('user_id', $request->user_id)->get()->count();
        $datenote = $note? $note->updated_at : '1999-01-01 00:00:00';

        if($note){
            if($datenote != $request->date || $count!=$request->num) $result = 'sync';
            else $result = 'no';
        } else $result = 'sync';

        Helper::recordApiLog(null, 'V3 note/check', 'OK');
        return $this->jsonResponse(true, 200, 'Succesfully Check Note Update', $result);
    }
}
