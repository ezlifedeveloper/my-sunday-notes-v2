<?php

namespace App\Http\Controllers\api\v1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class NoteController extends Controller
{
    //
    public function getRecent(Request $request)
    {
        $id_user = $request->id_user;

        $note = Note::where('user_id', $id_user)
            ->selectRaw('*, user_id as id_user, 1 as state')
            ->orderBy('date', 'desc')
            ->orderBy('created_at', 'desc')
            ->limit(3)->get();
        $notez = Note::where('user_id', $id_user)->count();
        $note->makeVisible(['created_at', 'updated_at'])->toArray();

        $more = $notez>3 ? true : false;

        Helper::recordApiLog($id_user, 'V2 note/recent', 'OK');
        return response()->json(['status'=> 200, 'result'=>'Get Recent Note List Success', 'data'=>$note, 'more'=>$more]);
    }

    public function getData(Request $request)
    {
        $id = $request->id;
        $id_user = $request->id_user;
        $search = $request->search;

        $note = Note::selectRaw('*, user_id as id_user, 1 as state')
        ->where('user_id', '=' ,$id_user)
        ->when($id, function ($query) use ($id) {
            $query->where('id', $id);
        })->when($search, function ($query) use ($search) {
            $query->where( function ( $query ) use ($search) {
                $query->where('title', 'like', '%'.$search.'%');
                $query->orWhere('location', 'like', '%'.$search.'%');
                $query->orWhere('speaker', 'like', '%'.$search.'%');
                $query->orWhere('verse', 'like', '%'.$search.'%');
            });
        })->orderBy('date', 'desc')->orderBy('created_at', 'desc')->get();
        $note->makeVisible(['created_at', 'updated_at'])->toArray();

        Helper::recordApiLog($id_user, 'V2 note/data', 'OK');
        return response()->json(['status'=> 200, 'result'=>'Get Note List Success', 'data'=>$note]);
    }

    public function save(Request $request)
    {
        $note = Note::where('id', $request->id)
            ->update([
                'date' => $request->date,
                'user_id' => $request->id_user,
                'time' => $request->time,
                'location' => $request->location,
                'event' => $request->event,
                'speaker' => $request->speaker,
                'verse' => $request->verse,
                'note' => $request->note,
                'title' => $request->title,
                'created_by' => $request->id_user,
                'updated_by' => $request->id_user,
            ]);

        Helper::recordApiLog($request->id_user, 'V2 note/save', 'OK');
        return response()->json(['status'=> 200, 'result'=>'Note Saved', 'data'=>$note]);
    }

    public function create(Request $request)
    {
        $note = new Note;
        $note->date = $request->date;
        $note->user_id = $request->id_user;
        $note->time = $request->time;
        $note->location = $request->location;
        $note->event = $request->event;
        $note->speaker = $request->speaker;
        $note->verse = $request->verse;
        $note->note = $request->note;
        $note->title = $request->title;
        $note->created_by = $request->id_user;
        $note->updated_by = $request->id_user;
        $note->save();

        Helper::recordApiLog($request->id_user, 'V2 note/create', 'OK');
        return response()->json(['status'=> 200, 'result'=>'Note Saved', 'data'=>$note]);
    }

    public function sync(Request $request)
    {
        $iduser = $request->iduser;
        $data = json_decode($request->data);
        $update = 0;
        $insert = 0;
        Log::debug('syncing note of '.$iduser);
        
        foreach($data as $d)
        {
            if(isset($d->state) && !$d->state) continue;
            $check = Note::withTrashed()
                ->where('user_id', $iduser)
                ->where('title',$d->title)
                ->where('date',$d->date)
                ->where('time',$d->time)
                ->where('event',$d->event)
                ->where('location',$d->location)
                ->where('speaker',$d->speaker)
                ->first();
                
            if($check){
                // echo 'check '.$check->updated_at.' < '.$d->updated_at.' ?';
                if(isset($d->updated_at) && $check->updated_at<$d->updated_at){
                    // echo 'yes';
                    $update++;
                    $note = Note::where('id', $d->id)
                        ->update(['date' => $d->date,
                            'user_id' => $iduser,
                            'updated_by' => $iduser,
                            'created_by' => $iduser,
                            'time' => $d->time,
                            'location' => $d->location,
                            'event' => $d->event,
                            'speaker' => $d->speaker,
                            'verse' => $d->verse,
                            'note' => $d->note,
                            'title' => $d->title,
                            'deleted_at' => null
                        ]);
                }
            } else if(!$check) {
                $insert++;
                $notez = new Note;
                $notez->date = $d->date;
                $notez->user_id = $iduser;
                $notez->created_by = $iduser;
                $notez->updated_by = $iduser;
                $notez->time = $d->time;
                $notez->location = $d->location;
                $notez->event = $d->event;
                $notez->speaker = $d->speaker;
                $notez->verse = $d->verse;
                $notez->note = $d->note ? $d->note : "";
                $notez->title = $d->title;
                $notez->deleted_at = null;
                $notez->save();
            }
        }

        $info = 'insert '.$insert.' and update '.$update;
        Log::debug($info);
        $notez = Note::where('user_id',$iduser)->get();
        foreach($notez as $nz) {
            $nz->state = isset($nz->deleted_at) ? 0 : 1;
        }
        // dd($notez);
        Helper::recordApiLog($request->id_user, 'V2 note/sync', 'OK');
        return response()->json(['status'=> 200, 'result'=>'Sync Succeed', 'info'=>$info, 'data'=>$notez]);
    }
}