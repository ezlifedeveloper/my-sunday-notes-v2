<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Friend;
use DB;

    // Type
    // 0. Not found
    // 1. Belum friend
    // 2. Udah request
    // 3. Dia Request
    // 4. Udah teman

    // Status
    // Confirmed and Request

class FriendController extends Controller
{
    //
    public function searchUser(Request $request)
    {
        $search = $request->search;
        $user = User::select('id', 'name', 'username')
        ->where(function($query) use ($search) {
            $query->where('username', 'LIKE', '%'.$search.'%')
                ->orWhere('name', 'LIKE', '%'.$search.'%');
        })->where('username', 'NOT LIKE', $request->username)
        ->where('role', 'user')
        ->orderBy(DB::raw("CASE WHEN username LIKE '".$search."%' THEN 1
            WHEN username LIKE '%".$search."' THEN 3
            ELSE 2 END"))
        ->orderBy(DB::raw("CASE WHEN name LIKE '".$search."%' THEN 1
            WHEN name LIKE '%".$search."' THEN 3
            ELSE 2 END"))
        ->get();

        if($user == null) return response()->json(['status'=> 200, 'result'=>'User not found', 'data'=>$user]);
        else{
            foreach($user as $u){
                $req = Friend::where('username_1', $request->username)->where('username_2', $u->username)->first();
                if($req!=null) {
                    if($req->status == 'confirmed') $u->type='4';
                    else $u->type='2';
                    continue;
                } else $u->type='1';
                
                $req = Friend::where('username_2', $request->username)->where('username_1', $u->username)->first();
                if($req!=null) {
                    if($req->status == 'confirmed') $u->type='4';
                    else $u->type='3';
                } else $u->type='1';
            }

            return response()->json(['status'=> 200, 'result'=>'Search User Success', 'data'=>$user]);
        }
    }

    public function requestFriend(Request $request)
    {
        $friend = new Friend;
        $friend->userid_1 = $request->userid_1;
        $friend->userid_2 = $request->userid_2;
        $friend->username_1 = $request->username_1;
        $friend->username_2 = $request->username_2;
        $friend->status = 'request';
        $friend->save();

        return response()->json(['status'=> 200, 'result'=>'Friend Request Sent']);
    }

    public function confirmRequest(Request $request)
    {
        $friend = Friend::where('username_1', $request->username_1)
                    ->where('username_2', $request->username_2)
                        ->update(['status' => 'confirmed']);

        return response()->json(['status'=> 200, 'result'=>'Friend Request Confirmed']);
    }
    
    public function getRequesting(Request $request)
    {
        $friend = Friend::select('username_2 as username')->where('status', 'request')->where('username_1', $request->username)->get();
        $user = $this->convertUsernameToFriend($friend);

        return response()->json(['status'=> 200, 'result'=>'Get Request Success', 'data'=>$user]);
    }
    
    public function getRequested(Request $request)
    {
        $friend = Friend::select('username_1 as username')->where('status', 'request')->where('username_2', $request->username)->get();
        $user = $this->convertUsernameToFriend($friend);

        return response()->json(['status'=> 200, 'result'=>'Get Friend Request Success', 'data'=>$user]);
    }
    
    public function getFriendList(Request $request)
    {
        $username = $request->username;
        $friend1 = Friend::select('username_2 as username')->where('status', 'confirmed')
            ->where('username_1', $username)
            ->get();
        $friend2 = Friend::select('username_1 as username')->where('status', 'confirmed')
            ->where('username_2', $username)
            ->get();

        $friends = $friend1->merge($friend2);
        $user = $this->convertUsernameToFriend($friends);

        return response()->json(['status'=> 200, 'result'=>'Get Friend List Success', 'data'=>$user]);
    }

    private function convertUsernameToFriend($friends)
    {
        $friend = null;
        $i=0;
        foreach($friends as $f) {
            $friend[$i++] = $f->username;
        }

        if($friend!=null){
            $user = User::whereIn('username', $friend)->get();
            return $user;
        }

        return [];
    }
}
