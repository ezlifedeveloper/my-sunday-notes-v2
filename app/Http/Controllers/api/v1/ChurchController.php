<?php

namespace App\Http\Controllers\api\v1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Church;
use App\Models\ChurchRequest;
use App\Models\Note;
use Illuminate\Http\Request;

class ChurchController extends Controller
{
    //
    public function getData(Request $request)
    {
        $search = $request->search;
        $lat = $request->lat;
        $lng = $request->lng;
        $earths_radius = 6371;
        $distance = 100;

        $church = Church::selectRaw('*, (ACOS(COS(RADIANS(' . $lat . '))* COS(RADIANS(lat))* COS(RADIANS(lng)-RADIANS(' . $lng . '))+SIN(RADIANS(' . $lat . '))*SIN(RADIANS(lat)))*6371) AS distance')
            ->when($search, function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
                $query->orWhere('address', 'like', '%' . $search . '%');
                $query->orWhere('services', 'like', '%' . $search . '%');
            })->orderBy('distance', 'asc')->get();
        $church->makeVisible(['created_at', 'updated_at'])->toArray();

        foreach($church as $c) {
            if(strpos($c->image, env('APP_URL')) === false) $c->image = isset($c->image) ? env('APP_URL').$c->image : null;
        }

        Helper::recordApiLog(null, 'V2 church/data', 'OK');
        return response()->json(['status' => 200, 'result' => 'Get Church List Success', 'data' => $church]);
    }

    public function visit(Request $request)
    {
        $note = new Note;
        $note->date = $request->date;
        $note->user_id = $request->id_user;
        $note->time = $request->time;
        $note->location = $request->location;
        $note->event = $request->event;
        $note->created_by = $request->id_user;
        $note->updated_by = $request->id_user;
        $note->save();
        $note->makeVisible(['created_at', 'updated_at'])->toArray();

        Helper::recordApiLog($request->id_user, 'V2 church/visit', 'OK');
        return response()->json(['status' => 200, 'result' => 'Church Visit Recorded', 'data' => $note]);
    }

    public function request(Request $request)
    {
        //
        $church = new ChurchRequest();
        $church->name = $request->name;
        $church->phone = $request->phone;
        $church->address = $request->address;
        $church->services = $request->service;
        $church->lat = $request->lat;
        $church->lng = $request->lng;
        $church->type = $request->status;
        $church->user_id = $request->issued;
        $church->tracking = 'issued';

        if ($request->has('file')) {
            $ext = '.' . $request->file->getClientOriginalExtension();
            $filename = Helper::cleanStr($request->name).'-requested-'.time().$ext;
            $request->file->storeAs('public/church', $filename);
            $church->image = '/file/church/' . $filename;
        }

        $church->save();

        Helper::recordApiLog($request->id_user, 'V2 church/request', 'OK');
        return response()->json(['status' => 200, 'result' => 'Church Request Recorded', 'data' => $church]);
    }
}
