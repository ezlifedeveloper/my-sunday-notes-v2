<?php

namespace App\Http\Controllers\api\v1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Bible;
use App\Models\BibleList;
use App\Models\DailyVerse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BibleController extends Controller
{
    //
    public function getData(Request $request)
    {
        $version = BibleList::where('name', $request->version)->first();
        if(!$version) return Helper::jsonResponse(false, 500, 'Version not found'); 

        $bible = Bible::where('version_id', $version->id)->get();
        foreach($bible as $b) {
            $b->version_id = $version->name;
            $b->version_text = $version->text;
        }
        $bible->makeVisible(['created_at', 'updated_at'])->toArray();

        Helper::recordApiLog(null, 'V2 bible/data', 'OK');
        return response()->json(['status'=> 200, 'result'=>'Get Bible Data Success', 'data'=>$bible]);
    }

    public function checkVersion(Request $request)
    {
        $bible = BibleList::get()->count();
        $result = $request->num!=$bible ? 'sync' : 'no';
        $bible = BibleList::select('id', 'name as version_id', 'text as version_text', 'created_at', 'updated_at')->get();
        $bible->makeVisible(['created_at', 'updated_at'])->toArray();

        Helper::recordApiLog(null, 'V2 bible/version', 'OK');
        return response()->json(['status'=> 200, 'result'=>$result, 'data'=>$bible]);
    }

    public function getYearlyVerseData(Request $request)
    {
        $bible = DailyVerse::orderByRaw('rand()')->limit(366)->get();
        $bible->makeVisible(['created_at', 'updated_at'])->toArray();

        Helper::recordApiLog(null, 'V2 bible/dailyverse', 'OK');
        return response()->json(['status'=> 200, 'result'=>'Get Daily Verse Data Success', 'data'=>$bible]);
    }
}