<?php

namespace App\Http\Controllers\api\v1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Church;
use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    //
    public function getMainData(Request $request)
    {
        $iduser = $request->id_user;
        $lat = $request->lat;
        $lng = $request->lng;
        $earths_radius = 6371;
        $distance = 100;

        $nearby = Church::selectRaw('*, (ACOS(COS(RADIANS('.$lat.'))* COS(RADIANS(lat))* COS(RADIANS(lng)-RADIANS('.$lng.'))+SIN(RADIANS('.$lat.'))*SIN(RADIANS(lat)))*6371) AS distance')
                ->orderBy('distance', 'asc')->limit(1)->get();
        $nearby->makeVisible(['created_at', 'updated_at'])->toArray();
        // $nearby = DB::select(DB::raw('select *, (ACOS(COS(RADIANS('.$lat.'))* COS(RADIANS(m.lat))* COS(RADIANS(m.lng)-RADIANS('.$lng.'))
        //         +SIN(RADIANS('.$lat.'))*SIN(RADIANS(m.lat)))*6371) AS distance_in_km
        //         FROM churches m
        //         ORDER BY distance_in_km ASC
        //         limit 1')) ;
        $data['nearby'] = $nearby[0];
        // $data['nearby'] = Church::orderBy('id', 'desc')->first();
        // $data['lastweek'] = Note::where('id_user', $iduser)->orderBy('date', 'desc')->first();
        $data['countdif'] = Note::where('user_id', $iduser)->distinct('location')->count('location');
        $countmax = Note::select('location', DB::raw('count(*) as total'))->where('user_id', $iduser)->orderBy('total', 'desc')->groupBy('location')->first();
        if($countmax) $data['countmax'] = $countmax->location.'|'.$countmax->total;
        else $data['countmax'] = '-|0';

        Helper::recordApiLog($request->id_user, 'V2 maindata', 'OK');
        return response()->json(['status'=> 200, 'result'=>'Succesfully Get Main Data', 'data'=>$data]);
    }

    public function checkChurchUpdate(Request $request)
    {
        $church = Church::select('updated_at')->orderBy('updated_at', 'desc')->first();
        $count = Church::select('id')->get()->count();
        $datechurch = substr($church->updated_at, 0, 10);

        if($church){
            if($datechurch > $request->date || $count != $request->num) $result = 'sync down';
            else $result = 'no';
        } else $result = 'sync down';

        Helper::recordApiLog(null, 'V2 church/check', 'OK');
        return response()->json(['status'=> 200, 'result'=>'Succesfully Check Church Update', 'data'=>$result]);
    }

    public function checkNoteUpdate(Request $request)
    {
        Log::debug($request);
        $note = Note::select('updated_at')->where('user_id', $request->iduser)->orderBy('updated_at', 'desc')->first();
        $count = Note::select('id')->where('user_id', $request->iduser)->get()->count();
        $datenote = $note? $note->updated_at : '1999-01-01 00:00:00';

        if($note){
            if($datenote != $request->date || $count!=$request->num) $result = 'sync';
            else $result = 'no';
        } else $result = 'sync';
        Log::debug($result);

        Helper::recordApiLog(null, 'V2 note/check', 'OK');
        return response()->json(['status'=> 200, 'result'=>'Succesfully Check Note Update', 'data'=>$result]);
    }
}
