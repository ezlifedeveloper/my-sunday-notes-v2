<?php

namespace App\Http\Controllers\api\v1;

use App\Helpers\Helper;
use http\Env\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\ResetPassword;
use App\Mail\VerifyEmail;
use App\Models\User;

class UserController extends Controller
{
    //
    public function getData()
    {
        $user = User::all();

        Helper::recordApiLog(null, 'V2 user/data', 'OK');
        return response()->json(['status'=> 200, 'result'=>'Get User List Success', 'data'=>$user]);
    }

    public function register(Request $request)
    {
        $cekusername = User::where('username', $request->username)->first();
        if($cekusername){
            Helper::recordApiLog(null, 'V2 user/register', 'failed');
            return response()->json(['status' => 500, 'result'=>'Username already registered!']);
        }
        
        $cekemail = User::where('email', $request->email)->first();
        if($cekemail) {
            Helper::recordApiLog(null, 'V2 user/register', 'failed');
            return response()->json(['status'=>500, 'result'=>'Email already registered!']);
        }

        $user = new User;
        $user->name = $request->name;
        $user->username = $request->username;
        $user->birth_date = $request->birth_date;
        $user->sex = $request->sex;
        $user->email = $request->email;
        $user->role_id = 2;
        $user->password = bcrypt($request->password);
        $user->save();

        $hashed = bcrypt($request->email.$request->username);
        $url = env('APP_URL').'/verify?p='.$user->id.'&q='.$hashed;
        Mail::to($request->email)->send(new VerifyEmail($url));

        Helper::recordApiLog(null, 'V2 user/register', 'OK');
        return response()->json(['status'=> 200, 'result'=>'User Registered. Please check your email to verify your account']);
    }

    public function update(Request $request)
    {
        $user = User::where('id', $request->id)
            ->update(array_filter($request->all()));

        Helper::recordApiLog($request->id, 'V2 user/update', 'OK');
        return response()->json(['status'=> 200, 'result'=>'User Profile Saved', 'data'=>$user]);
    }

    private function generateToken($username, $email)
    {  
        $user = User::where('username', $username)->update(['api_token' => hash('sha256', $username.$email)]);
        return $username.$email;
    }

    public function login(Request $request)
    {
        $username = $request->username;
        $pwd = $request->password;
        
        $auth = User::where('username',$username)->get();
                        
        if($auth!=null && count($auth)>0 && Hash::check($pwd, $auth[0]->password) && $auth[0]->email_verified_at){
            $auth[0]['token'] = $this->generateToken($auth[0]->username, $auth[0]->email);
            Helper::recordApiLog($auth[0]->id, 'V2 login', 'OK');

            Helper::recordUserLogin($auth[0]->id, $auth[0]->username, 'ok', 'mobile');
            return response()->json(['status'=> 200, 'result'=>'Login Success', 'data'=>$auth[0]]);
        }
        else if($auth==null || count($auth)==0){
            Helper::recordApiLog(null, 'V2 login', 'OK');
            Helper::recordUserLogin(null, $auth[0]->username, 'ID not registered', 'mobile');
            return response()->json(['status'=> 500, 'result'=>'ID not registered']);
        }
        else if(!$auth[0]->email_verified_at){
            Helper::recordApiLog($auth[0]->id, 'V2 login', 'OK');
            Helper::recordUserLogin($auth[0]->id, $auth[0]->username, 'Account not verified', 'mobile');
            return response()->json(['status'=> 500, 'result'=>'Account not verified, please activate your account']);
        }
        else if(!Hash::check($pwd, $auth[0]->password)){
            Helper::recordApiLog(null, 'V2 login', 'OK');
            Helper::recordUserLogin(null, $auth[0]->username, 'Password and id combination not match', 'mobile');
            return response()->json(['status'=> 500, 'result'=>'Password and id combination not match']);
        }
    }

    public function loginGoogle(Request $request)
    {
        $cekemail = User::where('email', $request->email)->first();

	    if(!$cekemail)
        {
            $username = substr($request->email, 0, strpos($request->email, '@'));
            $cekusername = User::where('username', $username)->first();

            $tokztokz = $this->generateToken($username, $request->email);

            if($cekusername){
                $lastid = User::select('id')->orderBy('id', 'desc')->first();
                $username = $username.$lastid->id;
            } 

            $result = 'User Registered';
            $user = new User;
            $user->name = $username;
            $user->username = $username;
            $user->email = $request->email;
            $user->role_id = 2;
            // $user->password = bcrypt($request->password);
            $user->api_token = hash('sha256', $tokztokz);
            $user->email_verified_at = date("Y-m-d h:i:s", time());
            $user->save();

            $user->token = $tokztokz;
        }
	
        $result = 'Login Success';
        $data = User::where('email', $request->email)->first();
        // dd($data);
        
        $data->token = $this->generateToken($data->username, $data->email);
        Helper::recordApiLog($data->id, 'V2 google', 'OK');
        
        return response()->json(['status'=> 200, 'result'=>$result, 'data'=>$data]);
    }

    public function logout(Request $request)
    {
        $username = $request->username;
        
        $auth = User::where('username',$username)->update(['api_token'=>NULL]);
        $auth = User::where('username',$username)->first();
        Helper::recordApiLog($auth->id, 'V2 logout', 'OK');
                        
        return response()->json(['status'=> 200, 'result'=>'Logout Success']);
    }

    public function sendReset(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if($user) {
            $hashed = bcrypt($user->email.$user->username);
            $url = env('APP_URL').'/reset?p='.$user->id.'&q='.$hashed;
            Mail::to($request->email)->send(new ResetPassword($url));

            Helper::recordApiLog($user->id, 'V2 sendreset', 'OK');
            return response()->json(['status'=> 200, 'result'=>'Reset password request has been sent to your email']);
        }
        else {
            Helper::recordApiLog($user->id, 'V2 sendreset', 'Email not registered');
            return response()->json(['status'=> 500, 'result'=>'Email not registered']);
        }
    }
}