<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class VerifyTokenLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->header('userid') || !$request->header('Authorization'))
            return response()->json([
                'result' => false,
                'message' => 'ID User and Token are required',
            ], 400);

        $user = User::find($request->header('userid'));

        if (!$user)
            return response()->json([
                'result' => false,
                'message' => 'User Not Found',
            ], 404);

        if ($user->api_token == null) {
            return response()->json([
                'result' => false,
                'message' => 'You are not logged in',
            ], 400);
        }
        
        if ($user->api_token != $request->header('Authorization')) {
            return response()->json([
                'result' => false,
                'message' => 'Invalid Token',
            ], 401);
        }

        return $next($request);
    }
}
