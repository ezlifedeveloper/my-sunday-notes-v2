<?php

use App\Http\Controllers\api\v1\BibleController;
use App\Http\Controllers\api\v1\ChurchController;
use App\Http\Controllers\api\v1\FriendController;
use App\Http\Controllers\api\v1\MainController;
use App\Http\Controllers\api\v1\NoteController;
use App\Http\Controllers\api\v1\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Old API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')
    ->group(function () {
    Route::controller(UserController::class)
        ->group(function () {
        Route::post('login', 'login');
        Route::post('google', 'loginGoogle');
        Route::post('logout', 'logout');
        Route::post('sendreset', 'sendReset');
    });

    Route::controller(MainController::class)
        ->group(function () {
        Route::post('maindata', 'getMainData');
        Route::post('church/check', 'checkChurchUpdate');
        Route::post('note/check', 'checkNoteUpdate');
    });
    
    Route::prefix('user')
        ->group(function () {
        Route::controller(UserController::class)
            ->group(function () {
            Route::post('data', 'getData');
            Route::post('register', 'register');
            Route::post('update', 'update');
        });
    });

    Route::prefix('church')
        ->controller(ChurchController::class)
        ->group(function () {
        Route::post('data', 'getData');
        Route::post('visit', 'visit');
        Route::post('request', 'request');
    });

    Route::prefix('note')
        ->controller(NoteController::class)
        ->group(function () {
        Route::post('data', 'getData');
        Route::post('save', 'save');
        Route::post('create', 'create');
        Route::post('recent', 'getRecent');
        Route::post('sync', 'sync');
    });
});

Route::prefix('v2')
    ->group(function () {

    Route::controller(MainController::class)
        ->group(function () {
        Route::post('maindata', 'getMainData');
        Route::post('church/check', 'checkChurchUpdate');
        Route::post('note/check', 'checkNoteUpdate');
    });

    Route::prefix('church')
        ->controller(ChurchController::class)
        ->group(function () {
        Route::post('data', 'getData');
        Route::post('visit', 'visit');
        Route::post('request', 'request');
    });

    Route::prefix('bible')
        ->controller(BibleController::class)
        ->group(function () {
        Route::post('data', 'getData');
        Route::post('version', 'checkVersion');
        Route::post('dailyverse', 'getYearlyVerseData');
    });

    Route::prefix('note')
        ->controller(NoteController::class)
        ->group(function () {
        Route::post('data', 'getData');
        Route::post('save', 'save');
        Route::post('create', 'create');
        Route::post('recent', 'getRecent');
        Route::post('sync', 'sync');
    });
    
    Route::controller(UserController::class)
        ->group(function () {
        Route::post('google', 'loginGoogle');
        Route::post('login', 'login');
        Route::post('logout', 'logout');
        Route::post('sendreset', 'sendReset');

        Route::prefix('user')
            ->group(function () {
            Route::post('data', 'getData');
            Route::post('register', 'register');
            Route::post('update', 'update');
        });
    });

    Route::prefix('friend')
        ->controller(FriendController::class)
        ->group(function () {
        Route::post('search', 'searchUser');
        Route::post('request', 'requestFriend');
        Route::post('confirm', 'confirmRequest');
        Route::post('requested', 'getRequested');
        Route::post('requesting', 'getRequesting');
        Route::post('list', 'getFriendList');
    });
});