<?php

use App\Http\Controllers\LogoutController;
use App\Http\Livewire\Admin\AppReview;
use App\Http\Livewire\Admin\AppVersion;
use App\Http\Livewire\Admin\Church as AdminChurch;
use App\Http\Livewire\Admin\ChurchCollaborator;
use App\Http\Livewire\Admin\ChurchCreate;
use App\Http\Livewire\Admin\ChurchDashboard;
use App\Http\Livewire\Admin\ChurchRequest;
use App\Http\Livewire\Admin\Dashboard;
use App\Http\Livewire\Admin\Note as AdminNote;
use App\Http\Livewire\Admin\NoteCreate;
use App\Http\Livewire\Admin\Profile as AdminProfile;
use App\Http\Livewire\Admin\Sinode;
use App\Http\Livewire\Admin\User;
use App\Http\Livewire\Admin\UserRole;
use App\Http\Livewire\Front\Api;
use App\Http\Livewire\Auth\Login;
use App\Http\Livewire\Etc\GenerateAdk;
use App\Http\Livewire\Front\Career;
use App\Http\Livewire\Front\Church;
use App\Http\Livewire\Front\ChurchDetail;
use App\Http\Livewire\Front\Home;
use App\Http\Livewire\Front\Note;
use App\Http\Livewire\Front\Post;
use App\Http\Livewire\Front\PrivacyPolicy;
use App\Http\Livewire\Front\Profile;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', Home::class)->name('home');

Route::get('login', Login::class)->name('login');
Route::get('logout', LogoutController::class)->name('logout');

Route::name('front.')->group(function () {
    Route::get('api-dev', Api::class)->name('api');
    Route::get('career', Career::class)->name('career');
    Route::get('church', Church::class)->name('church');
    Route::get('church/{slug}', ChurchDetail::class)->name('church/{slug}');

    Route::get('note', Note::class)->name('note');
    Route::get('post', Post::class)->name('post');
    Route::get('profile', Profile::class)->name('profile');

    Route::get('privacy-policy', PrivacyPolicy::class)->name('privacy-policy');
});

Route::name('back.')->prefix('office')->middleware('auth')->group(function () {
    Route::middleware('hasRole:1')->group(function() {
        Route::get('dashboard', Dashboard::class)->name('dashboard');
    });
    Route::get('profile', AdminProfile::class)->name('profile');

    Route::middleware('hasRole:1')->group(function() {
        Route::get('church-dashboard', ChurchDashboard::class)->name('church-dashboard');
        Route::get('church-collaborator', ChurchCollaborator::class)->name('church-collaborator');
        Route::get('church-request', ChurchRequest::class)->name('church-request');
    });
    Route::middleware('hasRole:1,3')->group(function() {
        Route::get('church/create', ChurchCreate::class)->name('church-create');
        Route::get('church/edit/{dataId}', ChurchCreate::class)->name('church-edit/{dataId}');
        Route::get('sinode', Sinode::class)->name('sinode');
        Route::get('church', AdminChurch::class)->name('church');
    });

    Route::get('note', AdminNote::class)->name('note');

    Route::middleware('hasRole:1')->prefix('app')->name('app.')->group(function() {
        Route::get('review', AppReview::class)->name('review');
        Route::get('version', AppVersion::class)->name('version');
    });

    Route::middleware('hasRole:1')->group(function() {
        Route::get('user', User::class)->name('user');
        Route::get('role', UserRole::class)->name('role');
    });
});

Route::name('etc.')->prefix('etc')->group(function () {
    Route::get('generate-adk', GenerateAdk::class)->name('generate-adk');
});