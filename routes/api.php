<?php

use App\Http\Controllers\api\v3\BibleController;
use App\Http\Controllers\api\v3\ChurchController;
use App\Http\Controllers\api\v3\MainController;
use App\Http\Controllers\api\v3\NoteController;
use App\Http\Controllers\api\v3\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

require_once "old_api.php";

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v3')
    ->group(function () {

    Route::controller(UserController::class)
        ->group(function () {
        Route::post('google', 'loginGoogle');
        Route::post('login', 'login');

        Route::post('user/register', 'register');

        Route::middleware('verifyTokenLogin')
            ->group(function () {
            Route::post('logout', 'logout');
            Route::post('sendreset', 'sendReset');

            Route::prefix('user')
                ->group(function () {
                Route::post('data', 'getData');
                Route::post('update', 'update');
            });
        });
    });

    Route::controller(MainController::class)
        ->middleware('verifyTokenLogin')
        ->group(function () {
        Route::post('maindata', 'getMainData');
        Route::post('church/check', 'checkChurchUpdate');
        Route::post('note/check', 'checkNoteUpdate');
    });

    Route::prefix('church')
        ->controller(ChurchController::class)
        ->middleware('verifyTokenLogin')
        ->group(function () {
        Route::post('data', 'getData');
        Route::post('request', 'request');
    });

    Route::prefix('bible')
        ->controller(BibleController::class)
        ->middleware('verifyTokenLogin')
        ->group(function () {
        Route::get('data', 'getData');
        Route::get('version', 'checkVersion');
        Route::get('dailyverse', 'getYearlyVerseData');
    });

    Route::prefix('note')
        ->controller(NoteController::class)
        ->middleware('verifyTokenLogin')
        ->group(function () {
        Route::post('data', 'data');
        Route::post('save', 'save');
        Route::post('create', 'create');
    });
});